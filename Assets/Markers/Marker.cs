﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Marker : MonoBehaviour
{
    [System.Serializable]
    public class OnMarkerInitialized : UnityEvent<string> { }

    [SerializeField] int m_BaseDepartmentCount;
    [SerializeField] int m_DepartmentCount;
    [SerializeField] OnMarkerInitialized m_OnMarkerInitialized;
    [SerializeField] SpriteRenderer m_SpriteRenderer;
    [SerializeField] TMPro.TextMeshPro m_Text;

    [SerializeField] Sprite m_StandardSprite;
    [SerializeField] Sprite m_ClusterSprite;

    [SerializeField] float m_SmoothMoveTime = 1f;

    public bool ZoomIsBeingCalculated { get; set; }

    public bool Interactable { get; set; }

    public int TouchCount { get { return m_ClusterObjects.Count; } }

    bool m_IsTouching;

    [SerializeField] int m_ID;

    public int ID { get { return m_ID; } }

    public HashSet<Marker> Cluster { get { return m_ClusterObjects; } }
    public HashSet<Collider> ColliderCluster { get { return m_ColliderClusterObjects; } }

    HashSet<Marker> m_ClusterObjects = new HashSet<Marker>();
    HashSet<Collider> m_ColliderClusterObjects = new HashSet<Collider>();

    public bool IsInTheInspectedCluster { get; set; }
    public bool IsExitingTheInspectedCluster { get; set; }

    public SpriteRenderer SpriteRenderer { get { return m_SpriteRenderer; } }

    UnityEvent m_OnZoomStarted;
    UnityEvent m_OnZoomEnded;

    MarkerController.OnMarkerClicked m_OnMarkerClicked;
    System.Action<Marker> m_OnMarkerClickedReference;

    public int ClusterID { get; set; } = -1;

    MarkerController m_Controller;

    Vector3 m_InitialPosition;

    public int DepartmentCount
    {
        get
        {
            return m_DepartmentCount;
        }
        set
        {
            if (m_DepartmentCount == value)
                return;

            m_DepartmentCount = value;
            m_OnMarkerInitialized.Invoke(m_DepartmentCount < 2 ?
                string.Empty :
                m_DepartmentCount.ToString());
        }
    }

    public void Initialize(int id, MarkerController.POIposition poiData,
        UnityEvent onZoomStarted, UnityEvent onZoomEnded, MarkerController.OnMarkerClicked onMarkerClicked,
        System.Action<Marker> onMarkerClickedID, Vector3 initialPosition, MarkerController controller)
    {
        m_ID = id;

        m_SpriteRenderer.sortingOrder = -id;
        m_Text.sortingOrder = -id + 1;

        m_OnMarkerClicked = onMarkerClicked;
        m_OnZoomStarted = onZoomStarted;
        m_OnZoomEnded = onZoomEnded;

        m_DepartmentCount = poiData.Departments.Count;
        m_BaseDepartmentCount = m_DepartmentCount;

        m_InitialPosition = initialPosition;

        m_OnMarkerClickedReference = onMarkerClickedID;

        m_OnMarkerInitialized.Invoke(m_DepartmentCount < 2 ?
                string.Empty :
                m_DepartmentCount.ToString());
        m_Controller = controller;
    }

    public void GetClusterRecursive(HashSet<Marker> runningList)
    {
        if (!runningList.Contains(this))
        {
            runningList.Add(this);
        }
        else
        {
            return;
        }

        foreach (var marker in Cluster)
        {
            marker.GetClusterRecursive(runningList);
        }
    }

    public void SetVisible(bool visible)
    {
        bool textVisible = visible && (m_SpriteRenderer.color.a != 0);

        if (m_Controller.ZoomController.CurrentZoomLevel == 0)
            textVisible = false;

        m_Text.gameObject.SetActive(textVisible);
        m_SpriteRenderer.enabled = visible;
    }

    public void ResetDepartments(bool updateUI)
    {
        if (DepartmentCount != m_BaseDepartmentCount)
        {
            m_DepartmentCount = m_BaseDepartmentCount;
            if (updateUI)
            {
                m_OnMarkerInitialized.Invoke(m_DepartmentCount < 2 ?
                string.Empty :
                m_DepartmentCount.ToString());
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(this.tag))
        {
            var marker = other.GetComponent<Marker>();
            if (marker != null && !m_ClusterObjects.Contains(marker))
            {
                m_ClusterObjects.Add(marker);
                m_ColliderClusterObjects.Add(other);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!m_ColliderClusterObjects.Contains(other))
        {
            var marker = other.GetComponent<Marker>();
            if (marker != null && !m_ClusterObjects.Contains(marker))
            {
                m_ClusterObjects.Add(marker);
                m_ColliderClusterObjects.Add(other);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(this.tag))
        {
            var marker = other.GetComponent<Marker>();
            if (marker != null && m_ClusterObjects.Contains(marker))
            {
                m_ClusterObjects.Remove(marker);
                m_ColliderClusterObjects.Remove(other);
            }
        }
    }

    private void Update()
    {

        if (TouchCount > 0 && !m_IsTouching)
        {
            m_IsTouching = true;
            m_SpriteRenderer.sprite = m_ClusterSprite;
        }
        else if (TouchCount == 0 && m_IsTouching)
        {
            m_IsTouching = false;
            m_SpriteRenderer.sprite = m_StandardSprite;

            SetVisible(m_Controller.ZoomController.CurrentZoomLevel > 0);
            ResetDepartments(true);
        }
        m_Text.gameObject.SetActive(SpriteRenderer.color.a > 0.1f && m_Controller.ZoomController.CurrentZoomLevel > 0);


        if (IsInTheInspectedCluster)
        {
            m_SpriteRenderer.sprite = m_StandardSprite;
            ResetDepartments(true);
            return;
        }

        if (IsExitingTheInspectedCluster)
        {
            m_SpriteRenderer.sprite = m_ClusterSprite;
            return;
        }
    }

    public void MoveToPosition(Vector3 position)
    {
        StartCoroutine(SmoothMoveToPosition(position));
    }

    public void MoveToOriginalPosition()
    {
        StartCoroutine(SmoothMoveToPosition(m_InitialPosition));
    }

    IEnumerator SmoothMoveToPosition(Vector3 targetPos)
    {
        float progress = 0f;
        float step = Time.deltaTime / m_SmoothMoveTime;

        Vector3 startPosition = this.transform.position;

        while (progress < 1f)
        {
            progress += step;
            this.transform.position = Vector3.Lerp(startPosition, targetPos, progress);
            yield return null;
        }

        this.transform.position = targetPos;
    }

    Coroutine alphaCoroutine;

    public void SetAlpha(float alpha, float animationTimeMultiplier = 1f)
    {
        if (alphaCoroutine != null)
        {
            StopCoroutine(alphaCoroutine);
            alphaCoroutineRunning = false;
        }

        float maxAlpha = m_Controller.GetMarkerAlphaRelativeToCameraAngle(this);
        if (alpha > maxAlpha)
            alpha = maxAlpha;

        if (Mathf.Abs(SpriteRenderer.color.a - alpha) > .1f)
            alphaCoroutine = StartCoroutine(AlphaAnim(alpha, animationTimeMultiplier));
    }

    public void SetAlphaInstant(float alpha)
    {
        if (ClusterID != ID || alphaCoroutineRunning)
            return;

        if (SpriteRenderer.color.a != alpha)
            SpriteRenderer.color =
               new Color(SpriteRenderer.color.r, SpriteRenderer.color.g,
               SpriteRenderer.color.b, alpha);
    }

    bool alphaCoroutineRunning = false;
    IEnumerator AlphaAnim(float targetAlpha, float animationTimeMultiplier = 1f)
    {
        alphaCoroutineRunning = true;
        if (animationTimeMultiplier == 0f)
            animationTimeMultiplier = .1f;

        float progress = 0f;
        float initialAlpha = SpriteRenderer.color.a;
        while (progress < 1f)
        {
            progress += Time.deltaTime * 2f / animationTimeMultiplier;

            SpriteRenderer.color =
            new Color(SpriteRenderer.color.r, SpriteRenderer.color.g,
            SpriteRenderer.color.b, Mathf.Clamp(
                Mathf.Lerp(initialAlpha, targetAlpha, progress), 0f, 1f));
            yield return null;
        }
        progress = 1f;
        SpriteRenderer.color =
           new Color(SpriteRenderer.color.r, SpriteRenderer.color.g,
           SpriteRenderer.color.b, targetAlpha);

        m_Text.gameObject.SetActive(targetAlpha != 0f);

        alphaCoroutine = null;
        alphaCoroutineRunning = false;
    }


    // DEBUG ==========================
    int m_MaxWhileIterations = 1000000;


    private void OnMouseUpAsButton()
    {
        if (!Interactable)
            return;

        if (m_OnMarkerClickedReference != null)
            m_OnMarkerClickedReference.Invoke(this);

        if (m_OnMarkerClicked != null)
            m_OnMarkerClicked.Invoke(this.transform.position);

        HashSet<Marker> cluster = new HashSet<Marker>();
        GetClusterRecursive(cluster);

        foreach (var item in cluster)
        {
            item.IsInTheInspectedCluster = true;
        }

        if (TouchCount > 0 && !ZoomIsBeingCalculated)
        {
            StartCoroutine(CalculateZoom());
            m_Controller.InCluster = true;
        }
    }

    private IEnumerator CalculateZoom()
    {
        if (m_OnZoomStarted != null)
            m_OnZoomStarted.Invoke();

        HashSet<Marker> cluster = new HashSet<Marker>();
        GetClusterRecursive(cluster);

        Vector3 initalScale = this.transform.localScale;

        this.ZoomIsBeingCalculated = true;
        foreach (Marker clusterObject in cluster)
        {
            clusterObject.ZoomIsBeingCalculated = true;
        }

        bool clusterTouching = true;
        Vector3 scale;
        int iterator = 0;
        while (clusterTouching && iterator < m_MaxWhileIterations)
        {
            while (ZoomController.m_SmoothMoveProgress < .9f)
                yield return null;

            scale = this.transform.localScale;
            scale *= 1f - Time.deltaTime;
            this.transform.localScale = scale;

            foreach (Marker clusterObject in cluster)
            {
                clusterObject.transform.localScale = scale;
            }

            clusterTouching = false;
            foreach (Marker clusterObject in cluster)
            {
                clusterTouching |= clusterObject.TouchCount > 0;
            }

            clusterTouching |= TouchCount > 0;

            iterator++;
            yield return null;
        }

        yield return null;
        this.ZoomIsBeingCalculated = false;

        scale = this.transform.localScale;
        scale *= 1f - Time.deltaTime;
        this.transform.localScale = scale;

        foreach (Marker clusterObject in cluster)
        {
            clusterObject.transform.localScale = scale;
            clusterObject.ZoomIsBeingCalculated = false;
        }

        Debug.Log("Zoom factor : " + initalScale.x / transform.localScale.x +
            "\nTook " + iterator + " iterations.");

        if (m_OnZoomEnded != null)
            m_OnZoomEnded.Invoke();
    }
}
