﻿using UnityEngine;

public class FrontendPoiListManager : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject ManagerObject;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OpenDepartment(int myID)
    {
        ManagerObject.GetComponent<FrontEndGuiHandling>().OpenDepartmentFromID(myID);
    }
}
