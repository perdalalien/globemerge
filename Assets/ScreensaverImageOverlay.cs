﻿using System.IO;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
///  image overlay to show when screensaver is shown.
///  looks for image named screensavertext.png, in root of project.
///  if there it is shown on screensaver, if not, this object is destroyed.
/// </summary>
public class ScreensaverImageOverlay : MonoBehaviour
{
    // Start is called before the first frame update

    public RectTransform thisRectTransform;



    void Start()
    {

        string myPath = Application.dataPath;
        string _imagePath = myPath.Substring(0, myPath.LastIndexOf('/')) + "/";
        string myTargetFileName = _imagePath + "\\" + "screensavertext.png";

        if (File.Exists(myTargetFileName))
        {
            WWW www = new WWW("file:///" + myTargetFileName);
            RawImage img = gameObject.GetComponent<RawImage>();
            img.texture = www.texture;
            thisRectTransform.localScale = new Vector3(1f, 1f, 1f);

        }
        else
        {
            Destroy(gameObject);
        }
    }



    // Update is called once per frame
    void Update()
    {

    }


}
