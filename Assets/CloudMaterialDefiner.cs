﻿using UnityEngine;

public class CloudMaterialDefiner : MonoBehaviour
{
    // Start is called before the first frame update


    public float normal_strength = 0.5f;
    public float smoothness_specular = 0.5f;
    public float light_scale = 0.5f;
    public float reflection_shine = 0.22f;
    public float main_brightness = 0.5f;

    public float alpha_Modifier = 1.0f;

    public Color32 albedo_color = Color.white;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void updateCloudMaterial(GameObject myEarthObject)
    {
        StagitMaterialCloudChanger myCloudChangerScript = myEarthObject.GetComponentInChildren<StagitMaterialCloudChanger>();
        myCloudChangerScript.normal_strength = normal_strength;
        myCloudChangerScript.smoothness_specular = smoothness_specular;
        myCloudChangerScript.light_scale = light_scale;
        myCloudChangerScript.reflection_shine = reflection_shine;
        myCloudChangerScript.main_brightness = main_brightness;
        myCloudChangerScript.alpha_Modifier = alpha_Modifier;
        myCloudChangerScript.albedo_color = albedo_color;
        myCloudChangerScript.changeMaterials();

    }
}
