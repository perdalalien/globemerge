﻿using DigitalRuby.Tween;
using UnityEngine;


public class SQSQCloudHandling : MonoBehaviour
{
    private GameObject clouds;
    private GameObject cloudShadows;

    public static bool cloudFading = false;

    float visibleCloudsAlpha = -5.0f;
    float hiddenCloudsAlpha;
    float visibleShadowsAlpha;

    private StagitMaterialCloudChanger CloudMatrialHandler;
    private StagitMaterialCloudShadowChanger CloudShadowMaterialHandler;


    // Start is called before the first frame update
    void Start()
    {

    }

    private void initalize()
    {
        clouds = GameObject.Find("earthClouds");
        cloudShadows = GameObject.Find("CloudShadows");

        CloudMatrialHandler = clouds.GetComponent<StagitMaterialCloudChanger>();
        CloudShadowMaterialHandler = cloudShadows.GetComponent<StagitMaterialCloudShadowChanger>();
    }

    // Update is called once per frame
    void Update()
    {

    }



    public void fadeCLouds()
    {

        if (!clouds)
        {
            initalize();
        }

        if (visibleCloudsAlpha == -5.0f)
        {
            visibleCloudsAlpha = CloudMatrialHandler.alpha_Modifier;
            visibleShadowsAlpha = CloudShadowMaterialHandler.main_brightness;
            hiddenCloudsAlpha = 0.0f;
        }



        System.Action<ITween<float>> updateCLoudAlpha = (t) =>
        {

            CloudMatrialHandler.alpha_Modifier = t.CurrentValue;
            CloudMatrialHandler.changeMaterials();


        };

        System.Action<ITween<float>> updateCLoudShadowsAlpha = (t) =>
        {

            CloudShadowMaterialHandler.main_brightness = t.CurrentValue;
            CloudShadowMaterialHandler.changeMaterials();

        };

        System.Action<ITween<float>> cloudChangeFinnished = (t) =>
        {
            Debug.Log("clouds gone");

            cloudFading = false;
        };

        if (!cloudFading)
        {
            float endAlpha = visibleCloudsAlpha;
            float startAlpha = hiddenCloudsAlpha;
            float endShadowALpha = visibleShadowsAlpha;
            float startShadowsALpha = hiddenCloudsAlpha;

            if (clouds.GetComponent<StagitMaterialCloudChanger>().alpha_Modifier != 0.0f)
            {
                endAlpha = hiddenCloudsAlpha;
                startAlpha = visibleCloudsAlpha;
                endShadowALpha = hiddenCloudsAlpha;
                startShadowsALpha = visibleShadowsAlpha;
            }


            cloudFading = true;
            clouds.gameObject.Tween("clouds", startAlpha, endAlpha, 2.0f, TweenScaleFunctions.QuadraticEaseOut, updateCLoudAlpha, cloudChangeFinnished);
            cloudShadows.gameObject.Tween("cloudshadows", startShadowsALpha, endShadowALpha, 2.0f, TweenScaleFunctions.QuadraticEaseOut, updateCLoudShadowsAlpha);
        }
    }
}
