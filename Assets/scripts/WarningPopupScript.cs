﻿using UnityEngine;
using UnityEngine.UI;

public class WarningPopupScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject managerObject;

    public Text WarningText;
    public Button yesButton;
    public Button noButton;

    // 1: sure you want to delete
    private int _State;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowFileNotFound(string myFilePath)
    {
        _State = 0;
        string myString = "The following critical file could not be loaded: \n " + myFilePath + "\n we need to shut down";
        defineText(myString);
        DefineYesButton("shutdown");
        DefineNoButton("");
        noButton.GetComponent<Button>().interactable = false;
    }

    public void showDeletePOIWarning()
    {
        _State = 1;
        string myString = "Are you sure you want to delete this marker permanently?";
        defineText(myString);
        DefineYesButton("Delete!");
        DefineNoButton("Cancel");


    }

    public void showDeleteDepartmentWarning()
    {
        _State = 2;
        string myString = "Are you sure you want to delete this department permanently?";
        defineText(myString);
        DefineYesButton("Delete!");
        DefineNoButton("Cancel");


    }

    public void showDeleteImageWarning()
    {
        _State = 3;
        string myString = "Are you sure you want to delete this image permanently?";
        defineText(myString);
        DefineYesButton("Delete!");
        DefineNoButton("Cancel");


    }

    public void yesButtonClicked()
    {
        switch (_State)
        {
            case 0:
                Application.Quit();
                break;
            case 1:
                managerObject.GetComponent<BackEndGuiHandling>().ExecutePOIDeletion();
                break;
            case 2:
                managerObject.GetComponent<BackEndGuiHandling>().ExecuteDepartmentDeletion();
                break;
            case 3:
                managerObject.GetComponent<BackEndGuiHandling>().executeImageDeletion();
                break;
            default:
                break;
        }
    }

    public void noButtonClicked()
    {
        switch (_State)
        {
            case 0:
                Application.Quit();
                break;
            case 1:
                managerObject.GetComponent<BackEndGuiHandling>().DeletePOICanceled();
                break;
            case 2:
                managerObject.GetComponent<BackEndGuiHandling>().DeleteDepartmentCanceled();
                break;
            case 3:
                managerObject.GetComponent<BackEndGuiHandling>().DeleteImageCanceled();
                break;
            default:
                break;
        }
    }

    public void defineText(string myText)
    {
        WarningText.text = myText;
    }

    public void DefineYesButton(string myText)
    {
        yesButton.GetComponentInChildren<Text>().text = myText;
    }

    public void DefineNoButton(string myText)
    {
        noButton.GetComponentInChildren<Text>().text = myText;
    }
}
