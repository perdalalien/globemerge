﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class DepartmentInputManager : MonoBehaviour
{

    public GameObject manangerObject;
    private dataHandling _datahandlingScript;

    private BackEndGuiHandling _guiHandlingScript;

    public Button SaveButton;

    public Button EditImagesButton;

    public TMP_InputField HeadingInput;
    public TMP_InputField SubHeading;
    public TMP_InputField Contents;

    public TMP_InputField Country;
    public TMP_InputField City;
    public TMP_InputField Company;
    public TMP_InputField Employees;

    public TMP_Text Heading;
    public Toggle isActiveInput;


    public TMP_Text CurrentLanguageText;

    public int CurrentPOIID;

    private DepartmentData _currentDepartment = new DepartmentData();

    public bool IsNew;

    string _currentLanguage = "DK";



    //private dataHandling DataHandlingScript = 


    // Start is called before the first frame update
    void Start()
    {
        _datahandlingScript = manangerObject.GetComponent<dataHandling>();
        _guiHandlingScript = manangerObject.GetComponent<BackEndGuiHandling>();



    }

    // Update is called once per frame
    void Update()
    {

    }

    public void FillTextFields()
    {
        switch (_currentLanguage)
        {
            case "DK":
                HeadingInput.text = _currentDepartment.HeadingDK;
                SubHeading.text = _currentDepartment.SubHeadingDK;
                Contents.text = _currentDepartment.ContentsDK;
                Country.text = _currentDepartment.CountryDK;
                City.text = _currentDepartment.CityDK;
                Company.text = _currentDepartment.CompanyDK;
                Employees.text = _currentDepartment.EmployeesDK;
                break;
            case "UK":
                HeadingInput.text = _currentDepartment.HeadingUK;
                SubHeading.text = _currentDepartment.SubHeadingUK;
                Contents.text = _currentDepartment.ContentsUK;
                Country.text = _currentDepartment.CountryUK;
                City.text = _currentDepartment.CityUK;
                Company.text = _currentDepartment.CompanyUK;
                Employees.text = _currentDepartment.EmployeesUK;
                break;
            case "DE":
                HeadingInput.text = _currentDepartment.HeadingDE;
                SubHeading.text = _currentDepartment.SubHeadingDE;
                Contents.text = _currentDepartment.ContentsDE;
                Country.text = _currentDepartment.CountryDE;
                City.text = _currentDepartment.CityDE;
                Company.text = _currentDepartment.CompanyDE;
                Employees.text = _currentDepartment.EmployeesDE;
                break;
            default:
                break;
        }
    }

    public void SwitchLanguage(string myLanguage)
    {
        switch (_currentLanguage)
        {
            case "DK":
                _currentDepartment.HeadingDK = HeadingInput.text;
                _currentDepartment.SubHeadingDK = SubHeading.text;
                _currentDepartment.ContentsDK = Contents.text;
                _currentDepartment.CountryDK = Country.text;
                _currentDepartment.CityDK = City.text;
                _currentDepartment.CompanyDK = Company.text;
                _currentDepartment.EmployeesDK = Employees.text;
                break;
            case "UK":
                _currentDepartment.HeadingUK = HeadingInput.text;
                _currentDepartment.SubHeadingUK = SubHeading.text;
                _currentDepartment.ContentsUK = Contents.text;
                _currentDepartment.CountryUK = Country.text;
                _currentDepartment.CityUK = City.text;
                _currentDepartment.CompanyUK = Company.text;
                _currentDepartment.EmployeesUK = Employees.text;
                break;
            case "DE":
                _currentDepartment.HeadingDE = HeadingInput.text;
                _currentDepartment.SubHeadingDE = SubHeading.text;
                _currentDepartment.ContentsDE = Contents.text;
                _currentDepartment.CountryDE = Country.text;
                _currentDepartment.CityDE = City.text;
                _currentDepartment.CompanyDE = Company.text;
                _currentDepartment.EmployeesDE = Employees.text;
                break;
            default:
                break;
        }
        _currentLanguage = myLanguage;

        CurrentLanguageText.text = _currentLanguage;
        FillTextFields();
    }

    public void ImageButtonActive(bool myActive = true)
    {
        EditImagesButton.GetComponent<Button>().interactable = myActive;
        if (!myActive)
        {
            EditImagesButton.GetComponentInChildren<Text>().text = "";
        }
    }

    public void FillContents(DepartmentData oldDepartment)
    {
        if (oldDepartment != null)
        {
            _currentDepartment = oldDepartment;
        }

        IsNew = false;
        Heading.text = "Edit Department";


        FillTextFields();



        isActiveInput.isOn = _currentDepartment.IsActive;

        SaveButton.GetComponentInChildren<Text>().text = "Save changes";
        if (_currentDepartment.Images == null)
        {
            EditImagesButton.GetComponentInChildren<Text>().text = "Add image/video";
        }
        else
        {
            EditImagesButton.GetComponentInChildren<Text>().text = "Edit images/videos";
        }
    }


    public void Preview()
    {
        SwitchLanguage(_currentLanguage);
        _guiHandlingScript.PreviewDepartment(_currentDepartment, _currentLanguage);
    }

    public void AddDepartment()
    {
        _currentDepartment = new DepartmentData();
        _currentLanguage = "DK";


    }

    public void ClearFields()
    {
        Heading.text = "Add New Department";
        IsNew = true;
        HeadingInput.text = "";
        SubHeading.text = "";
        Contents.text = "";
        Country.text = "";
        City.text = "";
        Company.text = "";
        Employees.text = "";
        isActiveInput.isOn = true;
        _currentDepartment = new DepartmentData();
        SaveButton.GetComponentInChildren<Text>().text = "Save Department";
    }


    public void saveDepartmentInfo()
    {
        SwitchLanguage("DK");
        _currentDepartment.IsActive = isActiveInput.isOn;
        if (IsNew)
        {
            _currentDepartment.ID = _datahandlingScript.getNextDepartmentID();

            _datahandlingScript.addDepartmentAndSave(_currentDepartment, CurrentPOIID);
        }
        else
        {
            _datahandlingScript.UpdateDepartmentAndSave(_currentDepartment);
        }

        _guiHandlingScript.CloseAddDepartment();
        _guiHandlingScript.openDepartmentList();
    }

    public void AddPicture()
    {

        _guiHandlingScript.editImages();

    }

    public void CancelMarkerInput()
    {
        _guiHandlingScript.CloseAddDepartment();

        _guiHandlingScript.openDepartmentList();
    }















}
