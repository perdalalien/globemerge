﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;


public class dataHandling : MonoBehaviour
{
    private string _configPath = "";
    private string _SavePath = "";
    private string _backupPath = "";
    private string _imageFolder = "images";

    private string _positionFileName = "positions.json";

    private string _generalDataFileName = "Config.json";

    private string _departmentDataFileName = "departments.json";

    private string _imageDataFileName = "images.json";

    private DateTime LastBackupTime;

    public GameObject WarningWindow;


    public List<POIposition> allPositions = new List<POIposition>();
    public List<POIposition> filteredPositions = new List<POIposition>();

    public List<DepartmentData> allDepartments = new List<DepartmentData>();
    public List<DepartmentData> filteredDepartments = new List<DepartmentData>();

    public List<DepartmentImage> allImages = new List<DepartmentImage>();
    public List<DepartmentImage> filteredImages = new List<DepartmentImage>();

    public GeneralSettings generalSettings;

    public CameraMovement cameraMovementScript;

    // Start is called before the first frame update

    private void Awake()
    {
        if (loadGeneral())
        {


            _SavePath = generalSettings.DataFolder;
            _backupPath = generalSettings.BackupFolder;
            Debug.Log("Here is my savepath: " + _SavePath);

            LoadPositions();
            LoadDepartments();
            LoadImages();
        }
        else
        {

            Invoke("FAIL", 2f);

        }
    }

    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FAIL()
    {
        if (WarningWindow != null)
        {
            WarningWindow.GetComponent<WarningPopupScript>().ShowFileNotFound(_configPath);
            WarningWindow.SetActive(true);
            WarningWindow.transform.parent.gameObject.SetActive(true);
        }


    }


    private bool loadGeneral()
    {
        String myPath = Application.dataPath;
        _configPath = myPath.Substring(0, myPath.LastIndexOf('/')) + "/" + _generalDataFileName;
        Debug.Log("my general json Path  " + _configPath);
        if (File.Exists(_configPath))
        {
            StreamReader reader = new StreamReader(_configPath);

            string myJsonString = reader.ReadToEnd();
            reader.Close();

            generalSettings = JsonConvert.DeserializeObject<GeneralSettings>(myJsonString);

            Debug.Log("READ" + generalSettings.DataFolder);

        }
        else
        {
            return false;
        }
        if (generalSettings.LoadGlobe == 0)
        {
            if (SceneManager.GetActiveScene().buildIndex != 1)
            {
                SceneManager.LoadScene(1);
            }

        }
        else
        {
            Screen.fullScreen = true;
        }

        if (cameraMovementScript != null)
        {
            cameraMovementScript.InjectMovementValues(generalSettings.RubberBand, generalSettings.SnapDistance, generalSettings.ScreenPointDistance);
        }
        return true;

    }


    public void DeleteImageAndSave(int myID)
    {
        string ImageSavePath = _SavePath + _imageFolder;
        for (int i = 0; i < allImages.Count; i++)
        {
            if (allImages[i].ID == myID)
            {
                string myFilePath = ImageSavePath + "\\" + allImages[i].FileName;
                Debug.Log(myFilePath);
                File.Delete(myFilePath);
                deleteImageFile(allImages[i].FileName);
                allImages.RemoveAt(i);
                DeleteImageFromDepartments(myID);
                break;
            }
        }



        SaveImages();
        saveDepartments();
    }

    private void DeleteImageFromDepartments(int myID)
    {
        for (int i = 0; i < allDepartments.Count; i++)
        {
            DepartmentData myDepartment = allDepartments[i];
            if (myDepartment.Images != null)
            {
                if (myDepartment.Images.Contains(myID))
                {
                    myDepartment.Images.Remove(myID);
                    if (myDepartment.Images.Count == 0)
                    {
                        myDepartment.Images = null;
                    }
                    break;
                }
            }

        }
    }

    public void DeletePositionAndSave(int MyID)
    {
        for (int i = 0; i < allPositions.Count; i++)
        {
            if (allPositions[i].ID == MyID)
            {

                allPositions.RemoveAt(i);
                break;
            }
        }
        savePositions();
    }

    public void DeleteDepartmentAndSave(int myID)
    {
        for (int i = 0; i < allDepartments.Count; i++)
        {
            if (allDepartments[i].ID == myID)
            {
                allDepartments.RemoveAt(i);

                DeleteDepartmentFromPOI(myID);

                break;
            }
        }
        saveDepartments();
        savePositions();
    }

    private void DeleteDepartmentFromPOI(int myID)
    {
        for (int i = 0; i < allPositions.Count; i++)
        {
            POIposition myCurrentPosition = allPositions[i];
            if (myCurrentPosition.Departments.Contains(myID))
            {
                myCurrentPosition.Departments.Remove(myID);

                if (myCurrentPosition.Departments.Count == 0)
                {
                    myCurrentPosition.Departments = null;
                }
                break;
            }
        }
    }

    public void moveDepartment(int myDepartmentID, int myDirection)
    {
        for (int i = 0; i < allPositions.Count; i++)
        {
            POIposition myCurrentPosition = allPositions[i];
            if (myCurrentPosition.Departments.Contains(myDepartmentID))
            {
                if ((myDirection == -1) && (myCurrentPosition.Departments[0] == myDepartmentID))
                {
                    break;
                }
                else if (((myDirection == 1) && (myCurrentPosition.Departments[myCurrentPosition.Departments.Count - 1] == myDepartmentID)))
                {
                    break;
                }
                else
                {
                    int myCurrentListPosition = myCurrentPosition.Departments.IndexOf(myDepartmentID);
                    myCurrentPosition.Departments.RemoveAt(myCurrentListPosition);
                    if (myDirection == -1)
                    {
                        myCurrentPosition.Departments.Insert(myCurrentListPosition - 1, myDepartmentID);
                    }
                    else
                    {
                        myCurrentPosition.Departments.Insert(myCurrentListPosition + 1, myDepartmentID);
                    }
                }
                break;
            }
        }
        savePositions();
    }

    public void MoveImage(int myImageID, int myDirection)
    {
        for (int i = 0; i < allDepartments.Count; i++)
        {
            DepartmentData myCurrentDepartment = allDepartments[i];
            if (myCurrentDepartment.Images.Contains(myImageID))
            {
                if ((myDirection == -1) && (myCurrentDepartment.Images[0] == myImageID))
                {
                    break;
                }
                else if (((myDirection == 1) && (myCurrentDepartment.Images[myCurrentDepartment.Images.Count - 1] == myImageID)))
                {
                    break;
                }
                else
                {
                    int myCurrentListPosition = myCurrentDepartment.Images.IndexOf(myImageID);
                    myCurrentDepartment.Images.RemoveAt(myCurrentListPosition);
                    if (myDirection == -1)
                    {
                        myCurrentDepartment.Images.Insert(myCurrentListPosition - 1, myImageID);
                    }
                    else
                    {
                        myCurrentDepartment.Images.Insert(myCurrentListPosition + 1, myImageID);
                    }
                }
                break;
            }
        }
        saveDepartments();
    }

    public void AddpositionAndSave(POIposition myNewPosition)
    {
        allPositions.Add(myNewPosition);
        savePositions();
    }

    public void AddImageAndSave(DepartmentImage myNewImage, int myCurrentDepartmentID)
    {
        allImages.Add(myNewImage);
        for (int i = 0; i < allDepartments.Count; i++)
        {
            if (allDepartments[i].ID == myCurrentDepartmentID)
            {
                DepartmentData myCurrentDepartment = allDepartments[i];
                if (myCurrentDepartment.Images == null)
                {
                    myCurrentDepartment.Images = new List<int>();
                }
                myCurrentDepartment.Images.Add(myNewImage.ID);
            }

        }
        saveDepartments();
        SaveImages();
    }

    public void addDepartmentAndSave(DepartmentData MyNewDepartment, int myCurrentPOIID)
    {
        allDepartments.Add(MyNewDepartment);
        for (int i = 0; i < allPositions.Count; i++)
        {
            if (allPositions[i].ID == myCurrentPOIID)
            {
                POIposition myCurrentPosition = allPositions[i];
                if (myCurrentPosition.Departments == null)
                {
                    myCurrentPosition.Departments = new List<int>();
                }
                myCurrentPosition.Departments.Add(MyNewDepartment.ID);

            }
        }

        saveDepartments();
        savePositions();
    }

    private void saveDepartments()
    {
        string JsonString = JsonConvert.SerializeObject(allDepartments);
        Debug.Log(JsonString);
        SaveFiles(JsonString, _SavePath + _departmentDataFileName);
    }


    public void UpdateImageAndSave(DepartmentImage myOldImage)
    {
        for (int i = 0; i < allImages.Count; i++)
        {
            if (allImages[i].ID == myOldImage.ID)
            {
                DepartmentImage mySavedImage = allImages[i];
                mySavedImage.CommentDK = myOldImage.CommentDK;
                mySavedImage.CommentDE = myOldImage.CommentDE;
                mySavedImage.CommentUK = myOldImage.CommentUK;
                mySavedImage.FileName = myOldImage.FileName;
                mySavedImage.IsActive = myOldImage.IsActive;
                SaveImages();
                break;
            }
        }
    }

    public void UpdateDepartmentAndSave(DepartmentData myOldDepartment)
    {
        for (int i = 0; i < allDepartments.Count; i++)
        {
            if (allDepartments[i].ID == myOldDepartment.ID)
            {
                DepartmentData mySavedDepartment = allDepartments[i];
                mySavedDepartment.HeadingDK = myOldDepartment.HeadingDK;
                mySavedDepartment.SubHeadingDK = myOldDepartment.SubHeadingDK;
                mySavedDepartment.ContentsDK = myOldDepartment.ContentsDK;
                mySavedDepartment.CountryDK = myOldDepartment.CountryDK;
                mySavedDepartment.CityDK = myOldDepartment.CityDK;
                mySavedDepartment.CompanyDK = myOldDepartment.CompanyDK;
                mySavedDepartment.EmployeesDK = myOldDepartment.EmployeesDK;

                mySavedDepartment.HeadingUK = myOldDepartment.HeadingUK;
                mySavedDepartment.SubHeadingUK = myOldDepartment.SubHeadingUK;
                mySavedDepartment.ContentsUK = myOldDepartment.ContentsUK;
                mySavedDepartment.CountryUK = myOldDepartment.CountryUK;
                mySavedDepartment.CityUK = myOldDepartment.CityUK;
                mySavedDepartment.CompanyUK = myOldDepartment.CompanyUK;
                mySavedDepartment.EmployeesUK = myOldDepartment.EmployeesUK;

                mySavedDepartment.HeadingDE = myOldDepartment.HeadingDE;
                mySavedDepartment.SubHeadingDE = myOldDepartment.SubHeadingDE;
                mySavedDepartment.ContentsDE = myOldDepartment.ContentsDE;
                mySavedDepartment.CountryDE = myOldDepartment.CountryDE;
                mySavedDepartment.CityDE = myOldDepartment.CityDE;
                mySavedDepartment.CompanyDE = myOldDepartment.CompanyDE;
                mySavedDepartment.EmployeesDE = myOldDepartment.EmployeesDE;

                mySavedDepartment.IsActive = myOldDepartment.IsActive;
                saveDepartments();
                break;
            }
        }
    }

    public void UpdatePositionAndSave(POIposition myOldPosition)
    {
        for (int i = 0; i < allPositions.Count; i++)
        {
            if (allPositions[i].ID == myOldPosition.ID)
            {
                POIposition mySavedPosition = allPositions[i];
                mySavedPosition.Lat = myOldPosition.Lat;
                mySavedPosition.Lon = myOldPosition.Lon;
                mySavedPosition.Name = myOldPosition.Name;
                mySavedPosition.IsActive = myOldPosition.IsActive;
                savePositions();
                break;
            }
        }
    }


    private void LoadDepartments()
    {
        if (File.Exists(_SavePath + _departmentDataFileName))
        {
            StreamReader reader = new StreamReader(_SavePath + _departmentDataFileName);

            string myJsonString = reader.ReadToEnd();
            reader.Close();

            allDepartments = JsonConvert.DeserializeObject<List<DepartmentData>>(myJsonString);

            Debug.Log("READ" + allDepartments);

        }
    }

    public void SaveDepartments()
    {
        string JsonString = JsonConvert.SerializeObject(allDepartments);
        Debug.Log(JsonString);
        SaveFiles(JsonString, _SavePath + _departmentDataFileName);

    }

    private void LoadPositions()
    {
        if (Directory.Exists(_SavePath))
        {

            if (File.Exists(_SavePath + _positionFileName))
            {
                StreamReader reader = new StreamReader(_SavePath + _positionFileName);

                string myJsonString = reader.ReadToEnd();
                reader.Close();

                allPositions = JsonConvert.DeserializeObject<List<POIposition>>(myJsonString);
                Debug.Log("READ" + allPositions);


            }
        }
    }

    private void UpdateFilteredPositions()
    {
        filteredPositions.Clear();
        for (int i = 0; i < allPositions.Count; i++)
        {
            if (allPositions[i].IsActive)
            {
                filteredPositions.Add(allPositions[i]);
            }
        }
    }

    public string getFilteredPositionsAsString()
    {
        UpdateFilteredPositions();
        return JsonConvert.SerializeObject(filteredPositions);
    }

    public void savePositions()
    {
        Debug.Log(allPositions);
        string JsonString = JsonConvert.SerializeObject(allPositions);
        Debug.Log(JsonString);
        SaveFiles(JsonString, _SavePath + _positionFileName);

    }

    private void LoadImages()
    {
        if (Directory.Exists(_SavePath))
        {
            if (File.Exists(_SavePath + _imageDataFileName))
            {
                StreamReader reader = new StreamReader(_SavePath + _imageDataFileName);

                string myJsonString = reader.ReadToEnd();
                reader.Close();

                allImages = JsonConvert.DeserializeObject<List<DepartmentImage>>(myJsonString);

                Debug.Log("READ" + allPositions);

            }
        }
    }

    public void SaveImages()
    {
        string JsonString = JsonConvert.SerializeObject(allImages);
        Debug.Log(JsonString);
        SaveFiles(JsonString, _SavePath + _imageDataFileName);
    }

    public void SaveFiles(string myJsonString, string myFileName)
    {
        if (!Directory.Exists(_SavePath))
        {
            Directory.CreateDirectory(_SavePath);
            Debug.Log("Trying to create Save Path");
        }

        if (!File.Exists(myFileName))
        {
            FileStream myFile = File.Create(myFileName);
            myFile.Close();
        }
        StreamWriter writer = new StreamWriter(myFileName);
        writer.Write(myJsonString);
        writer.Close();

        Debug.Log(myFileName);
    }

    public string saveImagefiLe(string myPath)
    {
        string myFileName = System.DateTime.Now.ToFileTime().ToString();

        string ImageSavePath = _SavePath + _imageFolder;


        string myExtenstion = System.IO.Path.GetExtension(myPath);

        if (!Directory.Exists(ImageSavePath))
        {
            Directory.CreateDirectory(ImageSavePath);
        }

        string myTargetFileName = ImageSavePath + "\\" + myFileName + myExtenstion;

        if (!File.Exists(myTargetFileName))
        {
            File.Copy(myPath, myTargetFileName);
        }
        else
        {
            Debug.Log("file Allready Exists.");
            return "";
        }


        return myFileName + myExtenstion;
    }

    public Texture getTextureOfImage(string myFileName)
    {
        string ImageSavePath = _SavePath + _imageFolder;
        string myTargetFileName = ImageSavePath + "\\" + myFileName;
        WWW www = new WWW("file:///" + myTargetFileName);
        return www.texture;
    }

    public string getFullPathOfVideo(string myFileName)
    {
        string ImageSavePath = _SavePath + _imageFolder;
        return ImageSavePath + "\\" + myFileName;
    }

    public void deleteImageFile(String myFileName)
    {
        //maybe later
    }


    public List<DepartmentImage> GetImagDataFromDepartmentID(int myID)
    {
        List<DepartmentImage> myReturnList = new List<DepartmentImage>();

        for (int i = 0; i < allDepartments.Count; i++)
        {
            if (myID == allDepartments[i].ID)
            {
                List<int> myImageIDs = allDepartments[i].Images;
                for (int j = 0; j < myImageIDs.Count; j++)
                {
                    for (int k = 0; k < allImages.Count; k++)
                    {
                        if (myImageIDs[j] == allImages[k].ID)
                        {
                            myReturnList.Add(allImages[k]);
                            break;
                        }
                    }
                }
                break;
            }
        }

        return myReturnList;

    }

    public List<DepartmentData> GetDepartmentDataListFromPOIID(int myID)
    {
        List<DepartmentData> myReturnList = new List<DepartmentData>();
        for (int i = 0; i < allPositions.Count; i++)
        {
            if (myID == allPositions[i].ID)
            {
                List<int> myDepamentIDs = allPositions[i].Departments;
                for (int k = 0; k < myDepamentIDs.Count; k++)
                {
                    for (int j = 0; j < allDepartments.Count; j++)
                    {
                        if (myDepamentIDs[k] == allDepartments[j].ID)
                        {
                            myReturnList.Add(allDepartments[j]);
                            break;

                        }
                    }

                }
                break;
            }
        }
        return myReturnList;
    }

    public DepartmentData GetDepartmentFromID(int myDepartmentID)
    {
        for (int i = 0; i < allDepartments.Count; i++)
        {
            if (myDepartmentID == allDepartments[i].ID)
            {
                return allDepartments[i];
            }
        }
        return null;
    }


    public List<int> getImagesInDepartment(int myID)
    {
        List<int> myReturn = new List<int>();

        foreach (DepartmentData myDepartment in allDepartments)
        {
            if (myDepartment.ID == myID)
            {
                if (myDepartment.Images != null)
                {
                    return myDepartment.Images;
                }
            }
        }

        return myReturn;
    }

    public DepartmentImage GetImageFromID(int myID)
    {
        foreach (DepartmentImage myImage in allImages)
        {
            if (myImage.ID == myID)
            {
                return myImage;
            }
        }
        return null;
    }

    public List<int> getDepartmentsinPOI(int myID)
    {
        List<int> myReturn = new List<int>();

        foreach (POIposition myPosition in allPositions)
        {
            if (myPosition.ID == myID)
            {
                if (myPosition.Departments != null)
                {
                    return myPosition.Departments;
                }

            }
        }
        return myReturn;
    }

    public POIposition getPositionFromID(int myID)
    {
        foreach (POIposition myPosition in allPositions)
        {
            if (myPosition.ID == myID)
            {
                return myPosition;
            }
        }

        return null;
    }

    public int getNextPositionID()
    {
        int myCount;
        int myID;
        myCount = allPositions.Count;

        if (myCount == 0)
        {
            myID = 0;
        }
        else { myID = allPositions[myCount - 1].ID; }

        myID++;

        return myID;
    }

    public int getNextDepartmentID()
    {
        int myCount;
        int myID;
        myCount = allDepartments.Count;

        if (myCount == 0)
        {
            myID = 0;
        }
        else { myID = allDepartments[myCount - 1].ID; }

        myID++;

        return myID;
    }

    public int getNextImageID()
    {
        int myCount;
        int myID;
        myCount = allImages.Count;

        if (myCount == 0)
        {
            myID = 0;
        }
        else { myID = allImages[myCount - 1].ID; }

        myID++;

        return myID;
    }

    public void makeBackup(bool ForceIt = false)
    {
        if (Directory.Exists(_SavePath))
        {
            if ((LastBackupTime.Hour + 1 < DateTime.Now.Hour) || (ForceIt))
            {
                String myTagetFolderName = "backup_" + System.DateTime.Now.ToFileTime().ToString();
                DirectoryCopy(_SavePath, _backupPath + myTagetFolderName, true);
                LastBackupTime = DateTime.Now;
            }
            else
            {
                Debug.Log("its been less than one hour since last Backup");
            }
        }

    }

    private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
    {
        // Get the subdirectories for the specified directory.
        DirectoryInfo dir = new DirectoryInfo(sourceDirName);

        if (!dir.Exists)
        {
            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + sourceDirName);
        }

        DirectoryInfo[] dirs = dir.GetDirectories();
        // If the destination directory doesn't exist, create it.
        if (!Directory.Exists(destDirName))
        {
            Directory.CreateDirectory(destDirName);
        }

        // Get the files in the directory and copy them to the new location.
        FileInfo[] files = dir.GetFiles();
        foreach (FileInfo file in files)
        {
            string temppath = Path.Combine(destDirName, file.Name);
            file.CopyTo(temppath, false);
        }

        // If copying subdirectories, copy them and their contents to new location.
        if (copySubDirs)
        {
            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath, copySubDirs);
            }
        }
    }
}
