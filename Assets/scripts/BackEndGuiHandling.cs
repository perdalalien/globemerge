﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BackEndGuiHandling : MonoBehaviour
{

    public GameObject BackendCanvas;

    public GameObject NewPositionCanvas;
    public GameObject positionLinePrefab;
    public GameObject positionContentPanel;
    public GameObject positionList;

    public GameObject NewDepartmentCanvas;
    public GameObject departmentLinePrefab;
    public GameObject departmentContentPanel;
    public GameObject DepartmentsList;

    public GameObject NewImageCanvas;
    public GameObject ImageLinePrefab;
    public GameObject ImageContentPanel;
    public GameObject ImagesList;

    public GameObject WarningWindow;

    public GameObject FrontendDetailsView;
    public GameObject FrontendDepartmentList;
    public GameObject FrontendImageBig;

    public GameObject FrontendLanguageButtons;

    private List<GameObject> PoiListItems = new List<GameObject>();

    private List<GameObject> DepartmentsListItems = new List<GameObject>();

    private List<GameObject> ImagesLIstItems = new List<GameObject>();

    private POIposition _CurrentPosition;
    private DepartmentData _CurrentDepartment;
    private DepartmentImage _currentDepartmentImage;

    private dataHandling DatahandlingScript;

    private bool DebugIsOpen = false;

    int _currentPOI_ID;
    int _currentDepartment_ID;
    int _currentImage_ID;

    // Start is called before the first frame update
    void Start()
    {
        DatahandlingScript = gameObject.GetComponent<dataHandling>();
        FrontendLanguageButtons.GetComponent<FrontendLanguageButtonsHandler>().ShowHideButtons(true);

        closeAllDebug();

        Cursor.visible = true;

        //openPositionList();


    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.F1)))
        {
            if (DebugIsOpen)
            {
                closeAllDebug();
                DebugIsOpen = false;
                MakeBackup();
            }
            else
            {
                Cursor.visible = true;
                BackendCanvas.SetActive(true);
                openPositionList();
                DebugIsOpen = true;
                MakeBackup();
            }
        }
        else if (Input.GetKeyDown(KeyCode.F2))
        {
            MakeBackup(true);
        }
        else if (Input.GetKeyDown(KeyCode.F3))
        {
            Screen.fullScreen = !Screen.fullScreen;
        }
        else if (Input.GetKeyDown(KeyCode.F4))
        {
            Application.Quit();
        }
    }



    void closeAllDebug()
    {
        clearPoiList();
        BackendCanvas.SetActive(false);
        NewDepartmentCanvas.SetActive(false);
        NewPositionCanvas.SetActive(false);
        positionList.SetActive(false);
        WarningWindow.SetActive(false);
        DepartmentsList.SetActive(false);
        ImagesList.SetActive(false);
        NewImageCanvas.SetActive(false);

        FrontendDetailsView.SetActive(false);
        FrontendDepartmentList.SetActive(false);
        FrontendImageBig.SetActive(false);

    }

    public void clearPoiList()
    {
        for (int i = 0; i < PoiListItems.Count; i++)
        {
            GameObject myItem = PoiListItems[i];
            Destroy(myItem);
        }

        PoiListItems.Clear();
    }

    public void ClearDepartmentList()
    {
        for (int i = 0; i < DepartmentsListItems.Count; i++)
        {
            GameObject myItem = DepartmentsListItems[i];
            Destroy(myItem);
        }

        DepartmentsListItems.Clear();
    }

    public void ClearImagesList()
    {
        for (int i = 0; i < ImagesLIstItems.Count; i++)
        {
            GameObject myItem = ImagesLIstItems[i];
            Destroy(myItem);
        }
        ImagesLIstItems.Clear();
    }

    public void FillImagesList(int myID)
    {
        ClearImagesList();
        List<int> myImagesIDs = DatahandlingScript.getImagesInDepartment(myID);

        for (int i = 0; i < myImagesIDs.Count; i++)
        {
            GameObject myImageItem = Instantiate(ImageLinePrefab) as GameObject;
            myImageItem.transform.SetParent(ImageContentPanel.transform, false);

            DepartmentImage myImage = DatahandlingScript.GetImageFromID(myImagesIDs[i]);
            myImageItem.transform.localScale = Vector3.one;

            ImageListElement myScript = myImageItem.GetComponentInChildren<ImageListElement>();

            myScript.FillContents(myImage, DatahandlingScript.getTextureOfImage(myImage.FileName));

            ImagesLIstItems.Add(myImageItem);

        }
    }

    public void FillDepartmentList(int myID)
    {
        ClearDepartmentList();
        List<int> myDepartmentIDs = DatahandlingScript.getDepartmentsinPOI(myID);

        for (int i = 0; i < myDepartmentIDs.Count; i++)
        {
            GameObject myDepartmentItem = Instantiate(departmentLinePrefab) as GameObject;
            myDepartmentItem.transform.SetParent(departmentContentPanel.transform, false);

            DepartmentData myDepartment = DatahandlingScript.GetDepartmentFromID(myDepartmentIDs[i]);
            myDepartmentItem.transform.localScale = Vector3.one;

            DepartmentListElement myScript = myDepartmentItem.GetComponentInChildren<DepartmentListElement>();

            myScript.ID = myDepartment.ID;
            myScript.Name = myDepartment.HeadingDK;
            myScript.subHeading = myDepartment.SubHeadingDK;
            myScript.isActive = myDepartment.IsActive;


            myScript.UpdateText();

            if (myDepartment.Images != null)
            {
                myScript.CanBeDeleted = false;
            }
            else
            {
                myScript.CanBeDeleted = true;
            }
            myScript.UpdateDeleteButton();

            DepartmentsListItems.Add(myDepartmentItem);

        }



    }

    public void FillPoiList()
    {
        clearPoiList();

        for (int i = 0; i < DatahandlingScript.allPositions.Count; i++)
        {
            GameObject myPositionItem = Instantiate(positionLinePrefab) as GameObject;
            myPositionItem.transform.SetParent(positionContentPanel.transform, false);

            List<POIposition> myTempList = new List<POIposition>(DatahandlingScript.allPositions);

            myTempList.Sort(delegate (POIposition a, POIposition b)
            {
                return (a.Name).CompareTo(b.Name);
            });

            POIposition myPosition = myTempList[i];

            myPositionItem.transform.localScale = Vector3.one;
            POIListElement myScript = myPositionItem.GetComponentInChildren<POIListElement>();
            //  POIListElement myScript = myPositionItem.AddComponent<POIListElement>();
            myScript.ID = myPosition.ID;
            myScript.Name = myPosition.Name;
            if (myPosition.Departments != null)
            {
                myScript.CanBeDeleted = false;
            }
            else
            {
                myScript.CanBeDeleted = true;
            }
            myScript.UpdateDeleteButton();

            TMPro.TextMeshProUGUI myTextField = myPositionItem.GetComponentInChildren<TMPro.TextMeshProUGUI>();
            myTextField.SetText(myScript.Name);
            if (!myPosition.IsActive)
            {
                myTextField.fontStyle = FontStyles.Italic;
                myTextField.alpha = 0.5f;
            }

            PoiListItems.Add(myPositionItem);
            // Debug.Log("item added: " + myPosition.Name);
        }

        //Debug.Log("poilist: " + PoiListItems.Count + " allpos: " + DatahandlingScript.allPositions.Count);
        //Debug.Log(DatahandlingScript.allPositions);

    }

    public void addNewMarker()
    {
        positionList.SetActive(false);
        _CurrentPosition = new POIposition();
        NewPositionCanvas.GetComponent<PoiInputManager>().IsNew = true;
        OpenNewMarkerMenu();
    }

    public void CloseAddDepartment()
    {
        NewDepartmentCanvas.SetActive(false);
        FrontendDetailsView.SetActive(false);
    }

    public void CloseAddmarker()
    {
        NewPositionCanvas.SetActive(false);
    }

    public void openPositionList()
    {
        FillPoiList();
        positionList.SetActive(true);
    }

    public void openDepartmentList()
    {
        int myCount = DatahandlingScript.getDepartmentsinPOI(_currentPOI_ID).Count;
        if (myCount != 0)
        {
            FillDepartmentList(_currentPOI_ID);
            previewDepartmentList(_currentPOI_ID);
            DepartmentsList.SetActive(true);
        }
        else
        {
            openPositionList();
        }

    }

    public void OpenImageList()
    {
        FillImagesList(_currentDepartment_ID);
        ImagesList.SetActive(true);
        NewImageCanvas.SetActive(false);
    }

    public void OpenNewMarkerMenu()
    {
        NewPositionCanvas.GetComponent<PoiInputManager>().ClearFields();
        NewPositionCanvas.SetActive(true);
    }

    public void OpenNewDepartmentMenu()
    {
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().ClearFields();
        NewDepartmentCanvas.SetActive(true);
    }

    public void MoveDepartmentUp(int myID)
    {
        DatahandlingScript.moveDepartment(myID, -1);
        ClearDepartmentList();
        FillDepartmentList(_currentPOI_ID);
        previewDepartmentList(_currentPOI_ID);
    }

    public void MoveDepartmentDown(int myID)
    {
        DatahandlingScript.moveDepartment(myID, 1);
        ClearDepartmentList();
        FillDepartmentList(_currentPOI_ID);
        previewDepartmentList(_currentPOI_ID);
    }

    public void MoveImageUp(int myID)
    {
        DatahandlingScript.MoveImage(myID, -1);
        ClearImagesList();
        FillImagesList(_currentDepartment_ID);
    }

    public void MoveImageDown(int myID)
    {
        DatahandlingScript.MoveImage(myID, 1);
        ClearImagesList();
        FillImagesList(_currentDepartment_ID);
    }

    public void DeleteImage(int myID)
    {
        _currentImage_ID = myID;
        ImagesList.SetActive(false);
        WarningWindow.SetActive(true);
        WarningWindow.GetComponent<WarningPopupScript>().showDeleteImageWarning();
    }


    public void DeleteImageCanceled()
    {
        FillImagesList(_currentDepartment_ID);
        ImagesList.SetActive(true);
        WarningWindow.SetActive(false);
    }
    public void DeleteDepartment(int myID)
    {
        _currentDepartment_ID = myID;
        DepartmentsList.SetActive(false);
        WarningWindow.SetActive(true);
        WarningWindow.GetComponent<WarningPopupScript>().showDeleteDepartmentWarning();

    }

    public void executeImageDeletion()
    {
        DatahandlingScript.DeleteImageAndSave(_currentImage_ID);
        if (_CurrentDepartment.Images == null)
        {
            NewDepartmentCanvas.SetActive(true);
            NewDepartmentCanvas.GetComponent<DepartmentInputManager>().FillContents(_CurrentDepartment);
            WarningWindow.SetActive(false);
        }
        else
        {
            FillImagesList(_currentDepartment_ID);
            ImagesList.SetActive(true);
            WarningWindow.SetActive(false);
        }

    }
    public void ExecuteDepartmentDeletion()
    {
        DatahandlingScript.DeleteDepartmentAndSave(_currentDepartment_ID);
        FillDepartmentList(_currentPOI_ID);
        DepartmentsList.SetActive(true);
        WarningWindow.SetActive(false);
    }

    public void DeleteDepartmentCanceled()
    {
        FillDepartmentList(_currentPOI_ID);
        DepartmentsList.SetActive(true);
        WarningWindow.SetActive(false);
    }

    public void DeletePoi(int myID)
    {
        _currentPOI_ID = myID;
        positionList.SetActive(false);
        WarningWindow.SetActive(true);
        WarningWindow.GetComponent<WarningPopupScript>().showDeletePOIWarning();
    }


    public void ExecutePOIDeletion()
    {
        DatahandlingScript.DeletePositionAndSave(_currentPOI_ID);
        FillPoiList();
        positionList.SetActive(true);
        WarningWindow.SetActive(false);

    }


    public void DeletePOICanceled()
    {
        FillPoiList();
        positionList.SetActive(true);
        WarningWindow.SetActive(false);
    }

    public void EditPOI(int myID)
    {

        POIposition myPosition = DatahandlingScript.getPositionFromID(myID);
        if (myPosition != null)
        {
            NewPositionCanvas.GetComponent<PoiInputManager>().IsNew = false;
            OpenNewMarkerMenu();
            NewPositionCanvas.GetComponent<PoiInputManager>().fillContents(myPosition);
            positionList.SetActive(false);
        }
        else
        {
            Debug.LogError("position not FOUND!: " + myID);
        }
    }

    public void editImages()
    {
        _currentDepartment_ID = _CurrentDepartment.ID;
        if (_CurrentDepartment.Images == null)
        {
            AddNewImage();
        }
        else if (_CurrentDepartment.Images.Count == 0)
        {
            AddNewImage();
        }
        else
        {
            NewDepartmentCanvas.SetActive(false);
            FillImagesList(_currentDepartment_ID);
            ImagesList.SetActive(true);
        }
    }

    public void EditImage(int myImageID)
    {
        ImagesList.SetActive(false);
        _currentDepartmentImage = DatahandlingScript.GetImageFromID(myImageID);
        Texture myThumbTexture = null;
        if (_currentDepartmentImage.FileName != null)
        {
            myThumbTexture = DatahandlingScript.getTextureOfImage(_currentDepartmentImage.FileName);
        }
        NewImageCanvas.GetComponent<ImageInputManager>().fillContents(_currentDepartmentImage, myThumbTexture);
        NewImageCanvas.SetActive(true);
    }

    public void EditThisDepartment(int myID)
    {
        DepartmentsList.SetActive(false);
        _CurrentDepartment = DatahandlingScript.GetDepartmentFromID(myID);
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().IsNew = false;
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().CurrentPOIID = _currentPOI_ID;
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().ClearFields();
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().FillContents(_CurrentDepartment);

        NewDepartmentCanvas.SetActive(true);
        FrontendDepartmentList.SetActive(false);
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().ImageButtonActive(true);

        PreviewDepartment(_CurrentDepartment, "DK");

    }

    public void EditDepartment(int myID)
    {
        _currentPOI_ID = myID;
        positionList.SetActive(false);
        int myCount = DatahandlingScript.getDepartmentsinPOI(myID).Count;
        if (myCount != 0)
        {
            FillDepartmentList(_currentPOI_ID);
            previewDepartmentList(_currentPOI_ID);
            DepartmentsList.SetActive(true);
        }
        else
        {
            AddNewDepartment();
        }
    }

    public void closeAddNewImage()
    {
        NewImageCanvas.SetActive(false);
        FillImagesList(_currentDepartment_ID);
        ImagesList.SetActive(true);
    }

    public void AddNewDepartment()
    {
        DepartmentsList.SetActive(false);
        FrontendDepartmentList.SetActive(false);
        _CurrentDepartment = new DepartmentData();
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().IsNew = true;
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().CurrentPOIID = _currentPOI_ID;
        NewDepartmentCanvas.GetComponent<DepartmentInputManager>().ImageButtonActive(false);

        OpenNewDepartmentMenu();
    }

    public void AddNewImage()
    {
        NewDepartmentCanvas.SetActive(false);
        ImagesList.SetActive(false);
        _currentDepartmentImage = new DepartmentImage();
        NewImageCanvas.GetComponent<ImageInputManager>().ClearFields();
        NewImageCanvas.GetComponent<ImageInputManager>().IsNew = true;
        NewImageCanvas.GetComponent<ImageInputManager>().currentDepartmentID = _currentDepartment_ID;
        NewImageCanvas.SetActive(true);

    }

    public void closeDepartmentList()
    {
        DepartmentsList.SetActive(false);
        FrontendDepartmentList.SetActive(false);
        openPositionList();
    }

    public void CloseImageList()
    {
        ImagesList.SetActive(false);
        EditThisDepartment(_currentDepartment_ID);
    }


    public void MakeBackup(bool ForceIt = false)
    {
        DatahandlingScript.makeBackup(ForceIt);
    }

    public void PreviewDepartment(DepartmentData myData, string myLanguage)
    {
        FrontendDetailsView.SetActive(true);
        FrontendDetailsView.GetComponent<DetailViewContents>().UpdateContents(myData, myLanguage, true);
        FrontendLanguageButtons.GetComponent<FrontendLanguageButtonsHandler>().ForceLanguageUpdate();
        if (Screen.height == 1080)
        {
            FrontendDetailsView.GetComponentInChildren<RectTransform>().position = new Vector3(200f, Screen.height - 50, 0f);
            FrontendDetailsView.GetComponentInChildren<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 1.0f);
        }


    }

    public void previewDepartmentList(int myID)
    {
        FrontendDepartmentList.SetActive(true);
        List<DepartmentData> myDepartments = DatahandlingScript.GetDepartmentDataListFromPOIID(myID);
        FrontendDepartmentList.GetComponent<FrontendDepartmentListManager>().UpdateContents(myDepartments, true);
        if (Screen.height == 1080)
        {
            FrontendDepartmentList.GetComponentInChildren<RectTransform>().position = new Vector3(300f, Screen.height - 100, 0f);
        }
    }

}
