﻿using System.Collections.Generic;
using UnityEngine;

public class FrontendDepartmentListManager : MonoBehaviour
{

    public GameObject DepartmentPrefab;

    public GameObject DepartmentContentPanel;

    private List<GameObject> DepartmentsListItems = new List<GameObject>();
    private List<DepartmentData> CurrentDataList = new List<DepartmentData>();

    public GameObject LanguageHandlerObject;
    public FrontendLanguageButtonsHandler LanguageHandler;

    public string CurrentLanguage = "DK";

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        CurrentLanguage = LanguageHandler.CurrentLanguage;
        updateTextContents();
    }

    public void UpdateContents(List<DepartmentData> myDepartments, bool isDebug = false)
    {
        ClearDepartmentList();
        CurrentDataList.Clear();

        for (int i = 0; i < myDepartments.Count; i++)
        {
            GameObject myDepartmentItem = Instantiate(DepartmentPrefab) as GameObject;
            myDepartmentItem.transform.SetParent(DepartmentContentPanel.transform, true);
            myDepartmentItem.transform.localScale = Vector3.one;


            DepartmentListElement_FE myScript = myDepartmentItem.GetComponent<DepartmentListElement_FE>();

            if (isDebug)
            {
                myScript.DisableButton();
            }

            CurrentDataList.Add(myDepartments[i]);

            DepartmentsListItems.Add(myDepartmentItem);

            myScript.ID = myDepartments[i].ID;

            updateTextContents();

        }
        if (myDepartments.Count > 10)
        {
            this.GetComponent<RectTransform>().anchoredPosition = new Vector2(1980, -980);
        }
        else
        {
            this.GetComponent<RectTransform>().anchoredPosition = new Vector2(1980, -1080);
        }
    }

    private void updateTextContents()
    {
        for (int i = 0; i < DepartmentsListItems.Count; i++)
        {

            DepartmentListElement_FE myScript = DepartmentsListItems[i].GetComponent<DepartmentListElement_FE>();

            switch (CurrentLanguage)
            {
                case "DK":
                    myScript.Name = CurrentDataList[i].HeadingDK;
                    myScript.subHeading = CurrentDataList[i].SubHeadingDK;
                    break;
                case "UK":
                    myScript.Name = CurrentDataList[i].HeadingUK;
                    myScript.subHeading = CurrentDataList[i].SubHeadingUK;
                    break;
                case "DE":
                    myScript.Name = CurrentDataList[i].HeadingDE;
                    myScript.subHeading = CurrentDataList[i].SubHeadingDE;
                    break;
                default:
                    break;
            }


            myScript.UpdateText();
        }
    }



    public void ClearDepartmentList()
    {
        for (int i = 0; i < DepartmentsListItems.Count; i++)
        {
            GameObject myItem = DepartmentsListItems[i];
            Destroy(myItem);
        }

        DepartmentsListItems.Clear();
    }

    public void LanguageUpdate(string myLanguage)
    {
        CurrentLanguage = myLanguage;
        updateTextContents();
    }
}
