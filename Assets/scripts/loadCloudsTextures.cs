﻿using UnityEngine;

public class loadCloudsTextures : MonoBehaviour
{
    public bool loadTexturesOnExe = true;
    string colorMaterialFolder = "";
    // Start is called before the first frame update
    void Start()
    {

        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            if (loadTexturesOnExe)
            {
                colorMaterialFolder = "C:/G_globe/Clouds/";
                //loadLargeTextures();                
            }

        }
        else
        {

            Debug.Log("not loading textures");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void loadLargeTextures()
    {
        Debug.Log("Loading large textures here");
        Material[] thismaterials;
        thismaterials = this.GetComponent<Renderer>().sharedMaterials;

        string col_row;

        for (int i = 0; i < thismaterials.Length; i++)
        {
            col_row = thismaterials[i].name.Replace("earth", "");
            int row = int.Parse(col_row.Substring(0, 1));
            int col = int.Parse(col_row.Substring(2, 1));

            //-load large colorTexture//
            Texture2D newColorTexture = new Texture2D(4096, 4096, TextureFormat.ARGB32, true);
            string filePath = @"" + colorMaterialFolder + "clouds_" + row + "_" + col + ".png";
            string url = "file:///" + filePath;
            WWW www = new WWW(url);
            www.LoadImageIntoTexture(newColorTexture);
            thismaterials[i].SetTexture("_MainTex", newColorTexture);



        }


    }
}
