﻿using UnityEngine;

public class ImageContentsScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject ManagerObject;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void EditImage(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().EditImage(myID);
    }

    public void DeleteImage(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().DeleteImage(myID);
    }

    public void MoveUP(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().MoveImageUp(myID);
    }

    public void MoveDown(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().MoveImageDown(myID);
    }


}
