﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DepartmentListElement : MonoBehaviour
{
    // Start is called before the first frame update

    public int ID;
    public string Name;
    public string subHeading;
    public bool isActive = true;

    public TMP_Text NameField;
    public TMP_Text SubHeadingField;


    public GameObject DeleteButton;

    public bool CanBeDeleted = true;

    public void UpdateText()
    {
        NameField.SetText(Name);
        SubHeadingField.SetText(subHeading);
        if (!isActive)
        {
            NameField.fontStyle = FontStyles.Italic;
            SubHeadingField.fontStyle = FontStyles.Italic;
            NameField.alpha = 0.5f;
            SubHeadingField.alpha = 0.5f;
        }
    }


    public void UpdateDeleteButton()
    {
        if (!CanBeDeleted)
        {
            DeleteButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            DeleteButton.GetComponent<Button>().interactable = true;
        }
    }

    public void editThis()
    {

        GetComponentInParent<DepartmentContentsScript>().EditDepartment(ID);

    }

    public void DeleteThis()
    {
        GetComponentInParent<DepartmentContentsScript>().DeleteDepartment(ID);
    }

    public void MoveUp()
    {
        GetComponentInParent<DepartmentContentsScript>().MoveUp(ID);
    }


    public void MoveDown()
    {
        GetComponentInParent<DepartmentContentsScript>().MoveDown(ID);
    }




}
