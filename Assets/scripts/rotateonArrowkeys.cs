﻿using UnityEngine;

public class rotateonArrowkeys : MonoBehaviour
{

    public float rotateSpeed = .5f;
    public Transform verticalRotationObject;
    public Transform HorisontalRotationObject;

    private Vector3 _touchStartPos;
    private Vector3 _currentTouchPos;
    private Vector2 GlobeStartRotation;

    // Start is called before the first frame update
    void Start()
    {

    }




    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey(KeyCode.UpArrow))
        {
            verticalRotationObject.Rotate(0, 0, rotateSpeed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            verticalRotationObject.Rotate(0, 0, -rotateSpeed);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            HorisontalRotationObject.Rotate(0, rotateSpeed, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            HorisontalRotationObject.Rotate(0, -rotateSpeed, 0);
        }
    }


}
