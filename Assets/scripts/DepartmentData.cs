﻿using System.Collections.Generic;

public class DepartmentData
{
    public int ID { get; set; }

    public bool IsActive { get; set; }

    public string HeadingDK { get; set; }
    public string HeadingDE { get; set; }
    public string HeadingUK { get; set; }

    public string SubHeadingDK { get; set; }
    public string SubHeadingDE { get; set; }
    public string SubHeadingUK { get; set; }

    public string ContentsDK { get; set; }
    public string ContentsDE { get; set; }
    public string ContentsUK { get; set; }

    public string CountryDK { get; set; }
    public string CountryDE { get; set; }
    public string CountryUK { get; set; }

    public string CityDK { get; set; }
    public string CityDE { get; set; }
    public string CityUK { get; set; }

    public string CompanyDK { get; set; }
    public string CompanyDE { get; set; }
    public string CompanyUK { get; set; }

    public string EmployeesDK { get; set; }
    public string EmployeesDE { get; set; }
    public string EmployeesUK { get; set; }

    public List<int> Images { get; set; }
}
