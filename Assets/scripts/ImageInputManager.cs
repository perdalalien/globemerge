﻿using SimpleFileBrowser;
using TMPro;
using UnityEngine;
using UnityEngine.UI;






public class ImageInputManager : MonoBehaviour
{

    public GameObject manangerObject;
    private dataHandling _datahandlingScript;

    private BackEndGuiHandling _guiHandlingScript;

    public Button SaveButton;

    public int currentDepartmentID;


    public TMP_InputField NoteInput;
    public TMP_Text Heading;
    public RawImage ImageBox;
    public Toggle isActiveInput;

    public GameObject ImagePanel;

    public TMP_Text CurrentLanguageText;

    private Texture ImageBoxOriginalTexture;

    private DepartmentImage _CurrentImage = new DepartmentImage();

    private bool ImageChanged = false;

    public Image MovieIcon;

    private string _currentLanguage = "DK";
    public bool IsNew;

    string Path;




    //private dataHandling DataHandlingScript = 


    // Start is called before the first frame update
    void Start()
    {
        FileBrowser.SetFilters(true, new FileBrowser.Filter("Images", ".jpg", ".png"));
        _datahandlingScript = manangerObject.GetComponent<dataHandling>();
        _guiHandlingScript = manangerObject.GetComponent<BackEndGuiHandling>();
        ImageBoxOriginalTexture = ImageBox.texture;

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void LoadImage()
    {

        FileBrowser.ShowLoadDialog(OnSuccess, OnCancel);

    }


    public void FillTextFields()
    {
        switch (_currentLanguage)
        {
            case "DK":
                NoteInput.text = _CurrentImage.CommentDK;
                break;
            case "UK":
                NoteInput.text = _CurrentImage.CommentUK;
                break;
            case "DE":
                NoteInput.text = _CurrentImage.CommentDE;
                break;

        }
    }

    public void SwitchLanguage(string myLanguage)
    {
        switch (_currentLanguage)
        {
            case "DK":
                _CurrentImage.CommentDK = NoteInput.text;
                break;
            case "UK":
                _CurrentImage.CommentUK = NoteInput.text;
                break;
            case "DE":
                _CurrentImage.CommentDE = NoteInput.text;
                break;
            default:
                break;
        }
        _currentLanguage = myLanguage;

        CurrentLanguageText.text = _currentLanguage;
        FillTextFields();
    }

    public void OnSuccess(string path)
    {
        Path = path;
        string myExtenstion = System.IO.Path.GetExtension(Path);
        if (myExtenstion.ToUpper() != ".MP4")
        {
            UpdateImage();
        }
        else
        {
            UpdateVideo();
        }

    }
    public void OnCancel()
    {

    }

    void UpdateVideo()
    {
        ImageChanged = true;

        _CurrentImage.Type = 1;
        MovieIcon.enabled = true;
        ImageBox.enabled = false;


    }

    void UpdateImage()
    {
        float myHeight = 126.0f;
        float myWidth = 224.0f;
        ImageBox.texture = ImageBoxOriginalTexture;
        WWW www = new WWW("file:///" + Path);
        ImageBox.texture = www.texture;
        ResizeToParent(ImageBox, 0.01f);
        ImageChanged = true;
        _CurrentImage.Type = 0;
        MovieIcon.enabled = false;
        ImageBox.enabled = true;

    }




    public void fillContents(DepartmentImage myoldImage, Texture myImageTexture = null)
    {

        IsNew = false;
        Heading.text = "Edit Image/Video";

        NoteInput.text = myoldImage.CommentDK;
        isActiveInput.isOn = myoldImage.IsActive;
        // _CurrentImage.ID = myoldImage.ID;
        _CurrentImage = myoldImage;


        SaveButton.GetComponentInChildren<Text>().text = "Save changes";
        if (myoldImage.Type == 0)
        {
            if (myImageTexture != null)
            {
                ImageBox.texture = myImageTexture;
                ResizeToParent(ImageBox, 0.01f);
                ImageChanged = false;
                MovieIcon.enabled = false;
                ImageBox.enabled = true;
            }
        }
        else
        {
            MovieIcon.enabled = true;
            ImageBox.enabled = false;
            ImageChanged = false;
        }
    }



    private Vector2 ResizeToParent(RawImage image, float padding = 0f)
    {
        float w = 0, h = 0;
        // var parent = image.GetComponentInParent<RectTransform>();
        var parent = ImagePanel.GetComponent<RectTransform>();
        var imageTransform = image.GetComponent<RectTransform>();

        // check if there is something to do
        if (image.texture != null)
        {
            if (!parent) { return imageTransform.sizeDelta; } //if we don't have a parent, just return our current width;
            padding = 1 - padding;
            float ratio = image.texture.width / (float)image.texture.height;
            var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
            if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90)
            {
                //Invert the bounds if the image is rotated
                bounds.size = new Vector2(bounds.height, bounds.width);
            }
            //Size by height first
            h = bounds.height * padding;
            w = h * ratio;
            if (w > bounds.width * padding)
            { //If it doesn't fit, fallback to width;
                w = bounds.width * padding;
                h = w / ratio;
            }
        }
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
        return imageTransform.sizeDelta;
    }


    public void AddImage(DepartmentImage myNewImage)
    {
        _CurrentImage = new DepartmentImage();

    }

    public void ClearFields()
    {
        _CurrentImage = new DepartmentImage();
        ImageChanged = false;
        Heading.text = "Add New Image/Video";
        IsNew = true;
        NoteInput.text = "";
        isActiveInput.isOn = true;
        SaveButton.GetComponentInChildren<Text>().text = "Save Image";
        ImageBox.texture = null;
        MovieIcon.enabled = false;
    }

    public void EditImage(int myMarkerID)
    {

    }

    public void SaveImageInfo()
    {
        SwitchLanguage("DK");
        _CurrentImage.IsActive = isActiveInput.isOn;

        if (ImageChanged)
        {
            _CurrentImage.FileName = _datahandlingScript.saveImagefiLe(Path);
            if (_CurrentImage.FileName == "")
            {
                Debug.LogError("Something went wrogn in copying, filename allready existed");
            }
        }

        if (IsNew)
        {
            _CurrentImage.ID = _datahandlingScript.getNextImageID();
            _datahandlingScript.AddImageAndSave(_CurrentImage, currentDepartmentID);
        }
        else
        {
            _datahandlingScript.UpdateImageAndSave(_CurrentImage);
        }

        _guiHandlingScript.closeAddNewImage();


    }

    public void CancelImageInput()
    {
        _guiHandlingScript.OpenImageList();

    }


}