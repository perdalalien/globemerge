﻿using UnityEngine;

public class SQSQGPSPosition : MonoBehaviour
{
    public float _globeRadius = 98;
    public GameObject Marker;
    private GameObject _instance;
    public float _scale = 8.0f;
    public float _xRot = 0f;
    public float _yRot = 0f;
    public float _zRot = 0f;

    // Start is called before the first frame update
    void Start()
    {

        _instance = Instantiate(Marker);

        var pos = Quaternion.AngleAxis(12.530892f, -Vector3.up) * Quaternion.AngleAxis(55.678784f, -Vector3.right) * new Vector3(0, 0, 1) * _globeRadius;
        _instance.transform.position = new Vector3(pos.x, pos.y, pos.z);
        _instance.transform.localScale = new Vector3(_scale, _scale, _scale);
        _instance.transform.LookAt(Vector3.zero, new Vector3(_xRot, _yRot, _zRot));

    }

    // Update is called once per frame
    void Update()
    {



    }
}
