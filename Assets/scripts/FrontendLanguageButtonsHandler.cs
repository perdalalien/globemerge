﻿using UnityEngine;
using UnityEngine.UI;

public class FrontendLanguageButtonsHandler : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject DKButton;
    public Sprite DKNormTexture;
    public Sprite DKSelectedTexture;

    public GameObject UKButton;
    public Sprite UKNormTexture;
    public Sprite UKSelectedTexture;

    public GameObject DEButton;
    public Sprite DENormTexture;
    public Sprite DESelectedTexture;

    public GameObject DepartmentList;
    public GameObject DetailView;
    public GameObject smallImageViewer;
    public GameObject LargeImageViewer;


    private GameObject[] allRecievers;
    private GameObject[] allButtons;
    private Sprite[] allNormTextures;
    private Sprite[] allSelectedTextures;
    private string[] allLanguages = new string[] { "DK", "UK", "DE" };

    public string CurrentLanguage = "DK";

    private void Awake()
    {
        AppState.AddListener(ScreensaverTurnOnOff);
    }

    void Start()
    {
        allButtons = new GameObject[] { DKButton, UKButton };
        //allButtons = new GameObject[] { DKButton, UKButton, DEButton };
        allNormTextures = new Sprite[] { DKNormTexture, UKNormTexture, DENormTexture };
        allSelectedTextures = new Sprite[] { DKSelectedTexture, UKSelectedTexture, DESelectedTexture };
        allRecievers = new GameObject[] { DepartmentList, DetailView, smallImageViewer, LargeImageViewer };
        ButtonCLicked(CurrentLanguage);
        ShowHideButtons(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ScreensaverTurnOnOff(bool screenSaverEntered)
    {
        if (screenSaverEntered)
        {
            ShowHideButtons(false);
        }
        else
        {
            CurrentLanguage = "DK";
            ShowHideButtons(true);
        }
    }

    public void ShowHideButtons(bool myShow)
    {
        if (allButtons != null)
        {
            for (int i = 0; i < allButtons.Length; i++)
            {
                allButtons[i].SetActive(myShow);
            }
        }
      
    }

    public void ButtonCLicked(string myLanguage)
    {

        CurrentLanguage = myLanguage;
        for (int i = 0; i < allRecievers.Length; i++)
        {
            allRecievers[i].SendMessage("LanguageUpdate", CurrentLanguage, SendMessageOptions.DontRequireReceiver);
        }
        UpdateButtonGFX();
    }

    public void ForceLanguageUpdate()
    {
        ButtonCLicked(CurrentLanguage);
    }

    public void UpdateButtonGFX()
    {
        for (int i = 0; i < allButtons.Length; i++)
        {
            GameObject myButton = allButtons[i];

            if (allLanguages[i] == CurrentLanguage)
            {
                myButton.GetComponent<Image>().sprite = allSelectedTextures[i];
            }
            else
            {
                myButton.GetComponent<Image>().sprite = allNormTextures[i];
            }
        }
    }
}
