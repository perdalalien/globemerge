﻿using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class PoiInputManager : MonoBehaviour
{

    public GameObject manangerObject;
    private dataHandling _datahandlingScript;

    private BackEndGuiHandling _guiHandlingScript;

    public Button SaveButton;

    public TMP_InputField latInput;
    public TMP_InputField lonInput;
    public TMP_InputField NoteInput;
    public TMP_Text Heading;
    public Toggle isActiveInput;


    private POIposition _CurrentPosition = new POIposition();

    public bool IsNew;



    //private dataHandling DataHandlingScript = 


    // Start is called before the first frame update
    void Start()
    {
        _datahandlingScript = manangerObject.GetComponent<dataHandling>();
        _guiHandlingScript = manangerObject.GetComponent<BackEndGuiHandling>();



    }

    // Update is called once per frame
    void Update()
    {

    }



    public void CheckOnGooglemaps()
    {

        Application.OpenURL("https://www.google.com/maps/search/?q=" + latInput.text + "," + lonInput.text + "&ll=" + latInput.text + "," + lonInput.text + "&z=8");

        //https://www.google.com/maps/search/?api=1&query=47.5951518,-122.3316393
    }

    public void fillContents(POIposition myoldMarker)
    {

        IsNew = false;
        Heading.text = "Edit Marker";
        latInput.text = myoldMarker.Lat.ToString(CultureInfo.CreateSpecificCulture("en-US"));
        latInput.ForceLabelUpdate();
        lonInput.text = myoldMarker.Lon.ToString(CultureInfo.CreateSpecificCulture("en-US"));
        NoteInput.text = myoldMarker.Name;
        isActiveInput.isOn = myoldMarker.IsActive;
        _CurrentPosition.ID = myoldMarker.ID;

        SaveButton.GetComponentInChildren<Text>().text = "Save changes";
    }


    public void AddMarker(POIposition myNewMarker)
    {
        _CurrentPosition = new POIposition();

    }

    public void ClearFields()
    {
        Heading.text = "Add New Marker";
        IsNew = true;
        _CurrentPosition = new POIposition();
        latInput.text = "";
        lonInput.text = "";
        NoteInput.text = "";
        isActiveInput.isOn = true;
        SaveButton.GetComponentInChildren<Text>().text = "Save marker";
    }

    public void EditMarker(int myMarkerID)
    {

    }

    public void saveMarkerInfo()
    {

        if (CheckMarkerFields())
        {

            _CurrentPosition.Lat = float.Parse(latInput.text, CultureInfo.CreateSpecificCulture("en-US"));
            _CurrentPosition.Lon = float.Parse(lonInput.text, CultureInfo.CreateSpecificCulture("en-US"));
            _CurrentPosition.Name = NoteInput.text;
            _CurrentPosition.IsActive = isActiveInput.isOn;
            if (IsNew)
            {
                _CurrentPosition.ID = _datahandlingScript.getNextPositionID();
                _datahandlingScript.AddpositionAndSave(_CurrentPosition);
            }
            else
            {
                _datahandlingScript.UpdatePositionAndSave(_CurrentPosition);
            }

            _guiHandlingScript.CloseAddmarker();
            _guiHandlingScript.openPositionList();

        }
    }

    public void CancelMarkerInput()
    {
        _guiHandlingScript.CloseAddmarker();
        _guiHandlingScript.openPositionList();
    }

    private bool CheckMarkerFields()
    {
        int myAcum = 0;




        if (latInput.text != "")
        {
            myAcum++;
            latInput.GetComponent<Image>().color = new Color(1f, 1f, 1f);
        }
        else
        {
            latInput.GetComponent<Image>().color = new Color(1f, .5f, .5f);
        }

        if (lonInput.text != "")
        {
            myAcum++;
            lonInput.GetComponent<Image>().color = new Color(1f, 1f, 1f);
        }
        else
        {
            lonInput.GetComponent<Image>().color = new Color(1f, .5f, .5f);
        }

        if (NoteInput.text != "")
        {
            myAcum++;
            NoteInput.GetComponent<Image>().color = new Color(1f, 1f, 1f);
        }
        else
        {
            NoteInput.GetComponent<Image>().color = new Color(1f, .5f, .5f);
        }

        if (myAcum != 3)
        {
            Debug.Log("point data NOT entered correctly");
            return false;
        }
        else
        {
            Debug.Log("point data entered correctly");
            return true;
        }

    }


    public int FindNextPositionID()
    {
        return 1;
    }










}
