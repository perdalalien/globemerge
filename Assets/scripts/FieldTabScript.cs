﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FieldTabScript : MonoBehaviour
{

    public List<TMP_InputField> fields;
    int fieldIndexer = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnEnable()
    {
        fieldIndexer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (fieldIndexer == fields.Count - 1)
            {
                fieldIndexer = -1;
            }
            fieldIndexer++;
            fields[fieldIndexer].Select();
        }
    }
}
