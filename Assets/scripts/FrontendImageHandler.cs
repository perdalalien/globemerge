﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class FrontendImageHandler : MonoBehaviour
{
    // Start is called before the first frame update

    public RawImage ImageHolder;
    public TMP_Text ImageNote;
    public Image NoteBackground;
    public Image NoteBackground2Line;
    public Button NextButton;
    public Button PrevButton;
    public Button ExtraButton;

    public string CurrentLanguage = "DK";

    public GameObject ManagerObject;

    public dataHandling DataHandlingScript;
    public FrontEndGuiHandling guiHandling;

    public float Padding = 1.0f;

    public bool isBigArea = false;

    public VideoPlayer _videoPlayer;
    public AudioSource audioSource;

    public GameObject PlayButton;

    public InputController ScreensaverHandler;


    private List<DepartmentImage> CurrentImages;
    DepartmentImage CurrentImage;
    int CurrentImageNumber = 0;

    private List<Texture> ImageTextures = new List<Texture>();

    private bool VideoIsPlaying = false;
    private long CurrentFrame;
    private int DepartmentID;

    

    void Start()
    {

        PlayButton.SetActive(false);
    
    }


    private void OnDisable()
    {
        for (int i = 0; i < ImageTextures.Count; i++)
        {
            Texture myTexture = ImageTextures[i];
            Destroy(myTexture);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (VideoIsPlaying)
        {
            if (ScreensaverHandler != null)
            {
                ScreensaverHandler.ForceScreensaverDelay();
            }

        }
    }

    public void BuildContents(int myDepartmentID, int myCurrentImageNumber = 0, long myVideoFrame = 0)
    {
        if (DataHandlingScript == null)
        {
            DataHandlingScript = ManagerObject.GetComponent<dataHandling>();
        }

        ImageTextures = new List<Texture>();
        DepartmentID = myDepartmentID;
        CurrentImageNumber = myCurrentImageNumber;
        CurrentFrame = myVideoFrame;
        Debug.Log(CurrentFrame);
        CurrentImages = DataHandlingScript.GetImagDataFromDepartmentID(myDepartmentID);

        if (CurrentImages.Count > 1)
        {
            ShowButtons();
        }
        else
        {
            HideButtons();
        }

        for (int i = 0; i < CurrentImages.Count; i++)
        {
            if (CurrentImages[i].Type == 0)
            {
                ImageTextures.Add(DataHandlingScript.getTextureOfImage(CurrentImages[i].FileName));
            }
            else
            {
                ImageTextures.Add(null);
            }

        }

        FillImage();
    }

    public void NoImages()
    {

        CurrentImages = null;
        ImageHolder.enabled = false;
        ImageHolder.texture = null;
        ImageTextures = new List<Texture>();
        HideButtons();
        ImageNote.text = "";
        ImageNote.enabled = false;
        Debug.Log("Imagenote disabled");
        NoteBackground.enabled = false;
        NoteBackground2Line.enabled = false;        
        //NoteBackground.rectTransform.sizeDelta = new Vector2(0, ImageNote.rectTransform.rect.height);
        
        if (!isBigArea)
        {
            ExtraButton.GetComponent<Button>().interactable = false;
        }
    }

    private void ShowButtons()
    {
        PrevButton.GetComponent<Button>().interactable = true;
        NextButton.GetComponent<Button>().interactable = true;
        ExtraButton.GetComponent<Button>().interactable = true;

    }

    private void HideButtons()
    {
        PrevButton.GetComponent<Button>().interactable = false;
        NextButton.GetComponent<Button>().interactable = false;
        ExtraButton.GetComponent<Button>().interactable = false;
    }

    private void FillImage()
    {
        FillText();
        PlayButton.SetActive(false);
        if (CurrentImages[CurrentImageNumber].Type == 0)
        {
            VideoIsPlaying = false;

            if (ImageTextures[CurrentImageNumber] != null)
            {
                ImageHolder.enabled = true;
                ImageHolder.texture = ImageTextures[CurrentImageNumber];
                ResizeToDefinedObject(ImageHolder, gameObject, Padding);
                ResizeNoteBack();

            }
            else
            {
                ImageHolder.texture = null;
            }
            _videoPlayer.Stop();
        }
        else
        {


            VideoIsPlaying = true;

            _videoPlayer.prepareCompleted += (VideoPlayer source) =>
            {
                ImageHolder.texture = _videoPlayer.texture;
                ResizeToDefinedObject(ImageHolder, gameObject, Padding);
                ResizeNoteBack();
                _videoPlayer.frame = CurrentFrame;
                _videoPlayer.Play();

            };

            string myPath = DataHandlingScript.getFullPathOfVideo(CurrentImages[CurrentImageNumber].FileName);
            _videoPlayer.url = myPath;
            _videoPlayer.source = VideoSource.Url;
            _videoPlayer.isLooping = true;

            _videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
            _videoPlayer.EnableAudioTrack(0, true);
            _videoPlayer.SetTargetAudioSource(0, audioSource);


            _videoPlayer.Prepare();


        }
    }

    public void ImageClicked()
    {
        if (VideoIsPlaying)
        {
            if (_videoPlayer.isPaused)
            {
                _videoPlayer.Play();
                PlayButton.SetActive(false);
            }
            else
            {
                _videoPlayer.Pause();
                PlayButton.SetActive(true);
            }
        }
    }

    private void FillText()
    {

        ImageNote.text = "";
        if (CurrentImages != null)
        {
            if (CurrentImages.Count != 0)
            {

                DepartmentImage myCurrentImage = CurrentImages[CurrentImageNumber];

                switch (CurrentLanguage)
                {
                    case "DK":
                        if (myCurrentImage.CommentDK != null)
                        {
                            ImageNote.text = myCurrentImage.CommentDK;
                        }
                        break;
                    case "UK":
                        if (myCurrentImage.CommentUK != null)
                        {
                            ImageNote.text = myCurrentImage.CommentUK;
                        }
                        break;
                    case "DE":
                        if (myCurrentImage.CommentDE != null)
                        {
                            ImageNote.text = myCurrentImage.CommentDE;
                        }
                        break;


                    default:
                        break;
                }
                ImageNote.enabled = true;
                Debug.Log("Imagenote enabled");
            }
        }

    }

    public void LanguageUpdate(string myLanguage)
    {
        CurrentLanguage = myLanguage;
        FillText();
        ResizeNoteBack();
    }

    public void LanguageSwapped(string NewLanguage)
    {
        CurrentLanguage = NewLanguage;
        FillText();
        ResizeNoteBack();
    }

    private void ResizeNoteBack()
    {
        if (ImageNote.text != "")
        {
            Vector2 mySize = ImageNote.GetPreferredValues();
            if(mySize.x + 40 > ImageHolder.rectTransform.rect.width)
            {
                NoteBackground.enabled = false;
                NoteBackground2Line.enabled = true;
            }
            else
            {
                NoteBackground2Line.enabled = false;
                NoteBackground.enabled = true;
                NoteBackground.rectTransform.sizeDelta = new Vector2(mySize.x + 40, ImageNote.rectTransform.rect.height);
            }
            
            
          
        }
        else
        {
            NoteBackground.enabled = false;
        }

    }

    public void showNextImage()
    {
        if (CurrentImageNumber < CurrentImages.Count - 1)
        {
            CurrentImageNumber++;
        }
        else
        {
            CurrentImageNumber = 0;
        }
        CurrentFrame = 0;
        FillImage();
    }

    public void showPrevImage()
    {
        if (CurrentImageNumber == 0)
        {
            CurrentImageNumber = CurrentImages.Count - 1;
        }
        else
        {
            CurrentImageNumber--;
        }
        CurrentFrame = 0;
        FillImage();
    }

    public void CloseImage()
    {
        if (VideoIsPlaying)
        {

            guiHandling.closeBigImage(CurrentImageNumber, _videoPlayer.frame);
            _videoPlayer.Stop();

        }
        else
        {
            guiHandling.closeBigImage(CurrentImageNumber);
        }
    }

    public void ShowImageFullscreen()
    {
        if (VideoIsPlaying)
        {

            guiHandling.showBigImage(DepartmentID, CurrentImageNumber, _videoPlayer.frame);
            _videoPlayer.Stop();
        }
        else
        {
            guiHandling.showBigImage(DepartmentID, CurrentImageNumber);
        }

    }

    private Vector2 ResizeToDefinedObject(RawImage image, GameObject sizeDefinerObject, float padding = 0f)
    {
        float w = 0, h = 0;
        var parent = sizeDefinerObject.GetComponent<RectTransform>();
        var imageTransform = image.GetComponent<RectTransform>();

        // check if there is something to do
        if (image.texture != null)
        {
            if (!parent) { return imageTransform.sizeDelta; } //if we don't have a parent, just return our current width;
            padding = 1 - padding;
            float ratio = image.texture.width / (float)image.texture.height;
            var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
            if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90)
            {
                //Invert the bounds if the image is rotated
                bounds.size = new Vector2(bounds.height, bounds.width);
            }
            //Size by height first
            h = bounds.height + padding;
            w = h * ratio;
            if (w > bounds.width + padding)
            { //If it doesn't fit, fallback to width;
                w = bounds.width + padding;
                h = w / ratio;
            }
        }
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
        return imageTransform.sizeDelta;
    }


}
