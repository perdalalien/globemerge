﻿public class DepartmentImage
{
    public int ID { get; set; }

    public string CommentDK { get; set; }

    public string CommentUK { get; set; }

    public string CommentDE { get; set; }

    public string FileName { get; set; }

    public bool IsActive { get; set; }

    public int Type { get; set; }


}


