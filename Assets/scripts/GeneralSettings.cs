﻿public class GeneralSettings
{


    public string DataFolder { get; set; }

    public int LoadGlobe { get; set; }

    public string GlobeAssetsFolder { get; set; }
    public string BackupFolder { get; set; }

    public float TimeOutSec { get; set; }

    public float RubberBand { get; set; }
    public float SnapDistance { get; set; }
    public float ScreenPointDistance { get; set; }




}
