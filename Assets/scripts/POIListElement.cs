﻿using UnityEngine;
using UnityEngine.UI;

public class POIListElement : MonoBehaviour
{
    // Start is called before the first frame update

    public int ID;
    public string Name;
    public bool CanBeDeleted;

    public GameObject DeleteButton;

    public void UpdateDeleteButton()
    {
        if (!CanBeDeleted)
        {
            DeleteButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            DeleteButton.GetComponent<Button>().interactable = true;
        }
    }

    public void editThis()
    {

        GetComponentInParent<PoiContentsScript>().EditPoi(ID);

    }

    public void DeleteThis()
    {
        GetComponentInParent<PoiContentsScript>().DeletePoi(ID);
    }

    public void editFactories()
    {
        GetComponentInParent<PoiContentsScript>().EditDepartments(ID);
    }

}
