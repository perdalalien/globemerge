﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FrontEndGuiHandling : MonoBehaviour
{
    // Start is called before the first frame update

    public dataHandling thisDataHandler;

    public GameObject DepartmentDetailView;
    public DetailViewContents detailViewContents;

    public GameObject DepartmentList;
    public FrontendDepartmentListManager departmentListManager;

    public GameObject BigImageView;
    public FrontendImageHandler bigImageHandler;

    public GameObject LanguageButtons;
    public FrontendLanguageButtonsHandler LanguageButtonsHandler;

    public GameObject functionButtons;

    public GameObject BackgroundCloseButton;

    public GameObject ScreensaverImageOverlay;

    public GameObject ZoomImageOverlay;

    private bool GuiIsOpen = false;

    public ZoomController zoomController;
    public MarkerController markerController;
    public InputController inputcontroller;


    private void Awake()
    {
        AppState.AddListener(ScreensaverTurnOnOff);

    }

    private void OnDestroy()
    {
        AppState.RemoveListener(ScreensaverTurnOnOff);
    }

    void Start()
    {
        if (!Application.isEditor)
        {
            Cursor.visible = false;
        }


        DepartmentDetailView.SetActive(false);
        DepartmentList.SetActive(false);
        BigImageView.SetActive(false);
        if (BackgroundCloseButton != null)
        {
            BackgroundCloseButton.SetActive(false);
        }
        GuiIsOpen = false;

        if (ZoomImageOverlay != null)
        {
            ZoomImageOverlay.SetActive(false);
        }


        // uncomment when screensaver trigger works//
        // LanguageButtons.SetActive(false);
        //functionButtons.SetActive(false);
        //
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void ScreensaverTurnOnOff(bool ScreensaverIsActivated)
    {
        if (ScreensaverIsActivated)
        {
            // BackgroundButtonClicked();
            CloseAll();
           

        }

        if (ScreensaverImageOverlay != null)
        {
            ScreensaverImageOverlay.SetActive(ScreensaverIsActivated);
        }

    }

    private void UpdateLanguage()
    {
        LanguageButtonsHandler.ForceLanguageUpdate();
    }

    public void BackgroundButtonClicked()
    {
        if (Vector2.Distance(Input.mousePosition, new Vector2(1920f, 1080f)) > 100)
        {
            CloseAll();

        }
    }

    public void CloseUI()
    {
        closeElement(DepartmentDetailView);
        closeElement(DepartmentList);
        closeElement(ZoomImageOverlay);
        closeElement(BigImageView);
        if (BackgroundCloseButton != null)
        {
            closeElement(BackgroundCloseButton);
        }

        GuiIsOpen = false;
    }

    public void CloseAll()
    {
        CloseUI();

        if (!zoomController.ZoomingOutOfCluster)
            markerController.SetMarkersInteractable(true, false);
        if (zoomController.CurrentZoomLevel != 2)
            markerController.ShouldRecalculate = true;
        inputcontroller.SetInputBlocked(false);

    }

    void closeElement(GameObject myElement)
    {
        if (myElement != null)
        {
            if (myElement.activeSelf)
            {
                // SetCanvasGroupAlpha(myElement,0f);
                myElement.SetActive(false);
            }
        }
    }

    public void CloseButtonClicked()
    {
        CloseAll();
    }

    public void POIOpened(int PoiID)
    {
        if (!GuiIsOpen)
        {
            GuiIsOpen = true;
            UnityEngine.Resolution myRes = Screen.currentResolution;
            if (myRes.height < 2100)
            {
                DepartmentDetailView.GetComponentInChildren<RectTransform>().position = new Vector3(200f, Screen.height - 50, 0f);
                DepartmentDetailView.GetComponentInChildren<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 1.0f);
                DepartmentList.GetComponentInChildren<RectTransform>().position = new Vector3(200f, Screen.height - 50, 0f);
                DepartmentList.GetComponentInChildren<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 1.0f);
                BigImageView.GetComponentInChildren<RectTransform>().position = new Vector3(200f, Screen.height - 50, 0f);
                BigImageView.GetComponentInChildren<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 1.0f);
            }

            List<int> foundList = thisDataHandler.getDepartmentsinPOI(PoiID);
            if (foundList.Count == 1)
            {
                //OpenDetails
                if (BackgroundCloseButton != null)
                {
                    BackgroundCloseButton.SetActive(true);
                }


                DepartmentDetailView.SetActive(true);
                DepartmentData myCurrentDepartment = thisDataHandler.GetDepartmentFromID(foundList[0]);
                detailViewContents.UpdateContents(myCurrentDepartment);
                UpdateLanguage();



            }
            else
            {
                if (BackgroundCloseButton != null)
                {
                    BackgroundCloseButton.SetActive(true);
                }
                DepartmentList.SetActive(true);
                List<DepartmentData> myDepartments = thisDataHandler.GetDepartmentDataListFromPOIID(PoiID);
                departmentListManager.UpdateContents(myDepartments);
                UpdateLanguage();


            }
        }
    }

    public void OpenDepartmentFromID(int myDepartmentID)
    {
        if (BackgroundCloseButton != null)
        {
            BackgroundCloseButton.SetActive(true);
        }
        DepartmentDetailView.SetActive(true);
        DepartmentData myCurrentDepartment = thisDataHandler.GetDepartmentFromID(myDepartmentID);
        detailViewContents.UpdateContents(myCurrentDepartment);
        DepartmentList.SetActive(false);
        UpdateLanguage();


    }



    public void showBigImage(int myDepartmentID, int myImageNumber, long myVideoTime = 0)
    {

        DepartmentDetailView.SetActive(false);
        openElementAlpha(BigImageView);
        bigImageHandler.BuildContents(myDepartmentID, myImageNumber, myVideoTime);
        UpdateLanguage();
        BigImageView.SetActive(true);
    }

    public void closeBigImage(int myCurrentImageNumber, long myVideoFrame = 0)
    {

        DepartmentDetailView.SetActive(true);
        detailViewContents.showImageNum(myCurrentImageNumber, myVideoFrame);
        UpdateLanguage();
        //BigImageView.SetActive(false);
        SetCanvasGroupAlpha(BigImageView, 0f);

    }


    public void ScreensaverModeStarted()
    {

        CloseAll();

        ZoomImageOverlay?.SetActive(false);
        LanguageButtons.SetActive(false);
        functionButtons.SetActive(false);
        Resources.UnloadUnusedAssets();
    }

    public void ScreensaverModeEnded()
    {
        LanguageButtons.SetActive(true);
        functionButtons.SetActive(true);
    }

    public void Zoomstuff(bool ZoomIsStarted)
    {
        if (ZoomIsStarted)
        {
            openElementAlpha(ZoomImageOverlay);
        }
        else
        {
            SetCanvasGroupAlpha(ZoomImageOverlay, 0f);
        }

    }


    private void openElementAlpha(GameObject myElement)
    {
        myElement.SetActive(true);
        myElement.GetComponent<CanvasGroup>().alpha = 0f;
        StartCoroutine(AlphaAnim(myElement, 1f));

    }

    public void SetCanvasGroupAlpha(GameObject myObject, float alpha)
    {
        //myObject.SetActive(false);
        StartCoroutine(AlphaAnim(myObject, alpha));
    }

    IEnumerator AlphaAnim(GameObject myObject, float targetAlpha)
    {
        CanvasGroup targetCanvasGroup = myObject.GetComponent<CanvasGroup>();
        float progress = 0f;
        float initialAlpha = targetCanvasGroup.alpha;
        while (progress < 1f)
        {
            progress += Time.deltaTime * 2f;

            targetCanvasGroup.alpha = Mathf.Clamp(Mathf.Lerp(initialAlpha, targetAlpha, progress), 0f, 1f);

            yield return null;
        }
        progress = 1f;
        targetCanvasGroup.alpha = targetAlpha;
        if (targetAlpha == 0f)
        {
            targetCanvasGroup.alpha = 1f;
            myObject.SetActive(false);
        }


    }


}
