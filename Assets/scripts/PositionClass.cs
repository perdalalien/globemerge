﻿public class PositionClass
{


    public class Factory
    {
        public int ID { get; set; }

        public string HeadingDK { get; set; }
        public string HeadingDE { get; set; }
        public string HeadingUK { get; set; }

        public string SubHeadingDK { get; set; }
        public string SubHeadingDE { get; set; }
        public string SubHeadingUK { get; set; }

        public string ContentsDK { get; set; }
        public string ContentsDE { get; set; }
        public string ContentsUK { get; set; }

        public string CountryDK { get; set; }
        public string CountryDE { get; set; }
        public string CountryUK { get; set; }

        public string CityDK { get; set; }
        public string CityDE { get; set; }
        public string CityUK { get; set; }

        public string CompanyDK { get; set; }
        public string CompanyDE { get; set; }
        public string CompanyUK { get; set; }

        public int[] Images { get; set; }

    }

    public class factoryImages
    {
        public int ID { get; set; }

        public string filename { get; set; }

        public string ImageTextDK { get; set; }
        public string ImageTextDE { get; set; }
        public string ImageTextUK { get; set; }



    }

}