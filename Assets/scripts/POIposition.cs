﻿using System.Collections.Generic;

public class POIposition
{
    public int ID { get; set; }

    public float Lat { get; set; }

    public float Lon { get; set; }
    public List<int> Departments { get; set; }

    public string Name { get; set; }

    public bool IsActive { get; set; }


}
