﻿using UnityEngine;

public class PoiContentsScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject ManagerObject;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void EditPoi(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().EditPOI(myID);
    }

    public void DeletePoi(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().DeletePoi(myID);
    }

    public void EditDepartments(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().EditDepartment(myID);
    }

    public void makeBackup()
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().MakeBackup();
    }
}
