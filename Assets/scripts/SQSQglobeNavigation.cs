﻿using System;
using UnityEngine;

public class SQSQglobeNavigation : MonoBehaviour
{

    private const int SphereRadius = 100;
    private Vector3? _touchStartPos;
    private Vector3? _currentTouchPos;
    private float _touchstartTime;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touches.Length == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                _touchStartPos = getTouchHit(touch.position);
                if (_touchStartPos != null)
                {
                    Debug.Log(convertToLatLon((Vector3)_touchStartPos));
                }
                _touchstartTime = Time.time;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                HandleDrag(touch.position);
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                if (Time.time - _touchstartTime <= 0.5f)
                {
                    // Debug.Log("touch");
                }
                else
                {
                    Debug.Log("drag");
                }
            }


        }
    }

    private void HandleDrag(Vector3 touchPosition)
    {
        _currentTouchPos = getTouchHit(touchPosition);
        RotateCamera((Vector3)_touchStartPos, (Vector3)_currentTouchPos);
    }

    private void RotateCamera(Vector3 dragStartPosition, Vector3 dragEndPosition)
    {
        // in case the spehre model is not a perfect sphere..


        dragEndPosition = dragEndPosition.normalized * SphereRadius;
        dragStartPosition = dragStartPosition.normalized * SphereRadius;
        // calc a vertical vector to rotate around..
        var cross = Vector3.Cross(dragEndPosition, dragStartPosition);




        // calc the angle for the rotation..
        var angle = Vector3.SignedAngle(dragEndPosition, dragStartPosition, cross);



        // roatate around the vector..
        Camera.main.transform.RotateAround(transform.position, cross, angle);

        // Debug.Log(Camera.main.transform.localRotation);



    }

    private Vector3? getTouchHit(Vector3 checkPosition)
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(checkPosition), out hit))
        {
            return hit.point;
        }
        return null;
    }

    private Vector2 convertToLatLon(Vector3 checkPosition)
    {

        checkPosition.Normalize();

        //longitude = angle of the vector around the Y axis
        //-( ) : negate to flip the longitude (3d space specific )
        //- PI / 2 to face the Z axis
        var lng = Math.Atan2(-checkPosition.z, -checkPosition.x) - Math.PI / 2;

        //to bind between -PI / PI
        if (lng < -Math.PI) lng += Math.PI * 2;

        //latitude : angle between the vector & the vector projected on the XZ plane on a unit sphere

        //project on the XZ plane
        var p = new Vector3(checkPosition.x, 0, checkPosition.z);
        //project on the unit sphere
        p.Normalize();

        //commpute the angle ( both vectors are normalized, no division by the sum of lengths )
        var lat = Math.Acos(Vector3.Angle(p, checkPosition));

        //invert if Y is negative to ensure teh latitude is comprised between -PI/2 & PI / 2
        if (checkPosition.y < 0) lat *= -1;

        return new Vector2((float)lat, (float)lng);

    }
}
