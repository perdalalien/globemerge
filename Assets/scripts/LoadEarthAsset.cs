﻿using UnityEngine;

public class LoadEarthAsset : MonoBehaviour
{
    AssetBundle myLoadedBundle_sqsq;
    AssetBundle myLoaded_globe;
    AssetBundle myLoaded_earth;
    AssetBundle myLoaded_clouds;
    AssetBundle myLoaded_normals;
    public Transform earthContainer;
    Object prefab;
    GameObject EarthObject;

    public GameObject Manager;

    public EarthMaterialDefiner myMaterialDefinerScript;
    public CloudMaterialDefiner myCloudMaterialDefiner;


    // public string myPath;
    public string myAssetName;
    // Start is called before the first frame update
    void Start()
    {
        string myPath = Manager.GetComponent<dataHandling>().generalSettings.GlobeAssetsFolder;

        loadAssetsBundle(myPath);
        instantiatePrefabFromBundle(myAssetName);
    }

    void loadAssetsBundle(string BundlePath)
    {
        myLoaded_clouds = AssetBundle.LoadFromFile(BundlePath + "cloudsbundle");
        myLoaded_normals = AssetBundle.LoadFromFile(BundlePath + "earthnormalbundle");
        myLoaded_globe = AssetBundle.LoadFromFile(BundlePath + "globe");
        myLoaded_earth = AssetBundle.LoadFromFile(BundlePath + "earthbundle.earth");
        myLoadedBundle_sqsq = AssetBundle.LoadFromFile(BundlePath + "sqsqbundle");
        Debug.Log(myLoadedBundle_sqsq == null ? "Failed to load AssetsBundle" : "Assetsbundle load Succeded");
    }

    void instantiatePrefabFromBundle(string myBundleName)
    {
        prefab = myLoadedBundle_sqsq.LoadAsset(myBundleName);
        EarthObject = Instantiate(prefab, earthContainer) as GameObject;
        EarthObject.name = "HDglobe";
        EarthObject.transform.localRotation = Quaternion.Euler(0, -180, 0);
        myMaterialDefinerScript.UpdateMaterial(EarthObject);
        myCloudMaterialDefiner.updateCloudMaterial(EarthObject);



    }




}
