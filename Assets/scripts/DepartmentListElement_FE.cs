﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DepartmentListElement_FE : MonoBehaviour
{
    // Start is called before the first frame update

    public int ID;
    public string Name;
    public string subHeading;



    public Button OpenButton;
    public TMP_Text NameField;
    public TMP_Text SubHeadingField;


    public void UpdateText()
    {
        NameField.SetText(Name);
        SubHeadingField.SetText(subHeading);

    }

    public void DisableButton()
    {
        OpenButton.GetComponent<Button>().interactable = false;
    }


    public void OpenDepartment()
    {
        GetComponentInParent<FrontendPoiListManager>().OpenDepartment(ID);
    }














}
