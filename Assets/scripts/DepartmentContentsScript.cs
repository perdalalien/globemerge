﻿using UnityEngine;

public class DepartmentContentsScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject ManagerObject;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void EditDepartment(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().EditThisDepartment(myID);
    }

    public void DeleteDepartment(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().DeleteDepartment(myID);
    }


    public void MoveUp(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().MoveDepartmentUp(myID);
    }

    public void MoveDown(int myID)
    {
        ManagerObject.GetComponent<BackEndGuiHandling>().MoveDepartmentDown(myID);
    }

    public void OpenDepartment(int myID)
    {

    }
}
