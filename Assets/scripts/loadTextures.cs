﻿using UnityEngine;

public class loadTextures : MonoBehaviour
{
    public bool loadTexturesOnExe = true;
    string colorMaterialFolder = "";
    // Start is called before the first frame update
    void Start()
    {

        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            if (loadTexturesOnExe)
            {
                colorMaterialFolder = "C:/G_globe/ColorMap/";
                loadLargeTextures();
            }
            else
            {
                colorMaterialFolder = "C:/G_globe/ColorMap2/";
                loadLargeTextures();
            }

        }
        else
        {

            Debug.Log("not loading textures");
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void loadLargeTextures()
    {
        Debug.Log("Loading large textures here");
        Material[] thismaterials;
        thismaterials = this.GetComponent<Renderer>().sharedMaterials;

        string col_row;

        for (int i = 0; i < thismaterials.Length; i++)
        {
            col_row = thismaterials[i].name.Replace("earth", "");
            int row = int.Parse(col_row.Substring(0, 1));
            int col = int.Parse(col_row.Substring(2, 1));

            //-load large colorTexture//
            Texture2D newColorTexture = new Texture2D(8192, 8192, TextureFormat.ARGB32, true);
            string filePath = @"" + colorMaterialFolder + "earth_" + row + "_" + col + ".png";
            string url = "file:///" + filePath;
            WWW www = new WWW(url);
            www.LoadImageIntoTexture(newColorTexture);
            thismaterials[i].SetTexture("_MainTex", newColorTexture);

            //-load large NightTexture//
            Shader thisSHader = thismaterials[i].shader;
            Texture2D newNightTexture = new Texture2D(4096, 4096, TextureFormat.ARGB32, true);
            string NightfilePath = @"C:/G_globe/NightMap/earth_" + row + "_" + col + "_night.png";
            string Nighturl = "file:///" + NightfilePath;
            WWW Nightwww = new WWW(Nighturl);
            Nightwww.LoadImageIntoTexture(newNightTexture);
            thismaterials[i].SetTexture("_Lights", newNightTexture);


        }


    }
}
