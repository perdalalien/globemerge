﻿using UnityEngine;

public class CameraZoom : MonoBehaviour
{

    public float defaultFOV = 15;
    public float stepValue = .1f;

    private GameObject clouds;
    private GameObject cloudShadows;
    private GameObject sunLight;
    private GameObject supportLight;
    private float nextCloudChange = 0.0f;

    private Color cloudAmbient = new Color(0.005f, 0.01f, 0.06f);
    private Color noCloudAmbient = new Color(0.01f, 0.02f, 0.12f);

    private bool cloudFading = false;
    float visibleCloudsAlpha = -5.0f;
    float hiddenCloudsAlpha;
    float visibleShadowsAlpha;

    // Start is called before the first frame update
    void Start()
    {

    }





    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            defaultFOV += stepValue;
            Camera.main.fieldOfView = defaultFOV;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            defaultFOV -= stepValue;
            Camera.main.fieldOfView = defaultFOV;
        }
        else if (Input.GetKey(KeyCode.W))
        {
            defaultFOV += stepValue / 10f;
            Camera.main.fieldOfView = defaultFOV;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            defaultFOV -= stepValue / 10f;
            Camera.main.fieldOfView = defaultFOV;
        }
        else if (Input.GetKey(KeyCode.C))
        {
            if (Time.time > nextCloudChange)
            {

                sunLight = GameObject.Find("sunLight");
                supportLight = GameObject.Find("supportLight");




                GetComponent<SQSQCloudHandling>().fadeCLouds();


                //  sunLight.GetComponent<Light>().enabled = clouds.GetComponent<MeshRenderer>().enabled;
                //  supportLight.GetComponent<Light>().enabled = !clouds.GetComponent<MeshRenderer>().enabled;
                if (cloudShadows.GetComponent<MeshRenderer>().enabled)
                {
                    RenderSettings.ambientLight = cloudAmbient;
                }
                else
                {
                    RenderSettings.ambientLight = noCloudAmbient;
                }



                GameObject.Find("EarthObject").GetComponent<EarthMovement>().enabled = clouds.GetComponent<MeshRenderer>().enabled;

                nextCloudChange = Time.time + 0.5f;
            }
        }
        else if (Input.GetKey(KeyCode.L))
        {
            Transform CameraTransform = GameObject.Find("Main Camera").GetComponent<Transform>();
            Transform LightTransform = GameObject.Find("Directional Light").GetComponent<Transform>();



        }
    }
}
