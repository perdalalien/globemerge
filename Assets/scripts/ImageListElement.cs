﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImageListElement : MonoBehaviour
{
    // Start is called before the first frame update

    public int ID;

    public TMP_Text CommentField;
    public RawImage thumbnail;
    public bool CanBeDeleted;

    public GameObject SizeDefiner;

    public GameObject DeleteButton;

    public Image MovieIcon;


    public void FillContents(DepartmentImage myImageData, Texture myImageTexture = null)
    {
        ID = myImageData.ID;
        CommentField.text = myImageData.CommentDK;
        if (myImageData.Type == 0)
        {
            if (myImageTexture != null)
            {
                thumbnail.texture = myImageTexture;
                ResizeToParent(thumbnail, 0.01f);
            }
            thumbnail.enabled = true;
            MovieIcon.enabled = false;
        }
        else
        {
            thumbnail.enabled = false;
            MovieIcon.enabled = true;
        }


        if (!myImageData.IsActive)
        {
            CommentField.fontStyle = FontStyles.Italic;
            CommentField.alpha = 0.5f;
            thumbnail.color = new Color(thumbnail.color.r, thumbnail.color.g, thumbnail.color.b, 0.5f);
        }

    }

    private Vector2 ResizeToParent(RawImage image, float padding = 0f)
    {
        float w = 0, h = 0;
        var parent = SizeDefiner.GetComponent<RectTransform>();
        var imageTransform = image.GetComponent<RectTransform>();

        // check if there is something to do
        if (image.texture != null)
        {
            if (!parent) { return imageTransform.sizeDelta; } //if we don't have a parent, just return our current width;
            padding = 1 - padding;
            float ratio = image.texture.width / (float)image.texture.height;
            var bounds = new Rect(0, 0, parent.rect.width, parent.rect.height);
            if (Mathf.RoundToInt(imageTransform.eulerAngles.z) % 180 == 90)
            {
                //Invert the bounds if the image is rotated
                bounds.size = new Vector2(bounds.height, bounds.width);
            }
            //Size by height first
            h = bounds.height * padding;
            w = h * ratio;
            if (w > bounds.width * padding)
            { //If it doesn't fit, fallback to width;
                w = bounds.width * padding;
                h = w / ratio;
            }
        }
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
        imageTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
        return imageTransform.sizeDelta;
    }

    private void loadAndPLaceImage(string fileName)
    {

    }

    public void UpdateDeleteButton()
    {
        if (!CanBeDeleted)
        {
            DeleteButton.GetComponent<Button>().interactable = false;
        }
        else
        {
            DeleteButton.GetComponent<Button>().interactable = true;
        }
    }

    public void editThis()
    {

        GetComponentInParent<ImageContentsScript>().EditImage(ID);

    }

    public void DeleteThis()
    {
        GetComponentInParent<ImageContentsScript>().DeleteImage(ID);
    }

    public void MoveUp()
    {
        GetComponentInParent<ImageContentsScript>().MoveUP(ID);
    }


    public void MoveDown()
    {
        GetComponentInParent<ImageContentsScript>().MoveDown(ID);
    }



}
