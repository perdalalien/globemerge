﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class DetailViewContents : MonoBehaviour
{

    
    

    public TMP_Text Heading_TMP;
    public TMP_Text SubHeading_TMP;
    public TMP_Text Details_TMP;
    public TMP_Text Country_TMP;
    public TMP_Text City_TMP;
    public TMP_Text Company_TMP;
    public TMP_Text Employees_TMP;

    public TMP_Text Employees_TMP_Heading;
    public TMP_Text Country_TMP_Heading;
    public TMP_Text City_TMP_Heading;
    public TMP_Text Company_TMP_Heading;



    public GameObject LanguageHandlerObject;
    public FrontendLanguageButtonsHandler languageHandler;

    public GameObject ImageHandlingObject;
    public FrontendImageHandler ImageHandler;

    public Button CloseButton;

    private string CurrentLanguage = "DK";
    private DepartmentData CurrentData;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        CurrentLanguage = languageHandler.CurrentLanguage;
        updateTextContents();
    }

    public void LanguageUpdate(string myLanguage)
    {
        CurrentLanguage = myLanguage;
        updateTextContents();
    }

    private void updateTextContents()
    {
        //Hide companies text
        Company_TMP.gameObject.SetActive(false);
        Company_TMP_Heading.gameObject.SetActive(false);
        
        //
        if (CurrentData != null)
        {
            switch (CurrentLanguage)
            {
                case "DK":

                    Heading_TMP.text = CurrentData.HeadingDK;
                    SubHeading_TMP.text = CurrentData.SubHeadingDK;
                    Details_TMP.text = CurrentData.ContentsDK;
                    Country_TMP.text = CurrentData.CountryDK;
                    Country_TMP.GetComponent<RectTransform>().anchoredPosition = new Vector2(710, -780);
                    City_TMP.text = CurrentData.CityDK;
                    City_TMP.GetComponent<RectTransform>().anchoredPosition = new Vector2(691, -805);
                    
                    // Company_TMP.text = CurrentData.CompanyDK;
                    Employees_TMP.text = CurrentData.EmployeesDK;
                    if ((CurrentData.EmployeesDK != null)&& (CurrentData.EmployeesDK != ""))
                    {
                       
                        Employees_TMP_Heading.gameObject.SetActive(true);
                        Employees_TMP.GetComponent<RectTransform>().anchoredPosition = new Vector2(791, -829);
                    }
                    else
                    {
                        Employees_TMP_Heading.gameObject.SetActive(false);
                    }

                    Employees_TMP_Heading.text = "Medarbejdere:";
                    City_TMP_Heading.text = "By:";
                    Country_TMP_Heading.text = "Land:";
            

                    break;
                case "DE":

                    Heading_TMP.text = CurrentData.HeadingDE;
                    SubHeading_TMP.text = CurrentData.SubHeadingDE;
                    Details_TMP.text = CurrentData.ContentsDE;
                    Country_TMP.text = CurrentData.CountryDE;
                    City_TMP.text = CurrentData.CityDE;
                    //Company_TMP.text = CurrentData.CompanyDE;
                    Employees_TMP.text = CurrentData.EmployeesDE;
                    if ((CurrentData.EmployeesDE != null)&& (CurrentData.EmployeesDE != ""))
                    {
                    
                        Employees_TMP_Heading.gameObject.SetActive(true);
                    }
                    else
                    {
                        Employees_TMP_Heading.gameObject.SetActive(false);
                    }
                    Employees_TMP_Heading.text = "Angestellte:";
                    City_TMP_Heading.text = "Stadt:";
                    Country_TMP_Heading.text = "Land:";

                    break;
                case "UK":

                    Heading_TMP.text = CurrentData.HeadingUK;
                    SubHeading_TMP.text = CurrentData.SubHeadingUK;
                    Details_TMP.text = CurrentData.ContentsUK;
                    Country_TMP.text = CurrentData.CountryUK;
                    Country_TMP.GetComponent<RectTransform>().anchoredPosition = new Vector2(742, -780);
                    City_TMP.text = CurrentData.CityUK;
                    City_TMP.GetComponent<RectTransform>().anchoredPosition = new Vector2(706, -805);
                    // Company_TMP.text = CurrentData.CompanyUK;
                    Employees_TMP.text = CurrentData.EmployeesUK;
                    if ((CurrentData.EmployeesUK != null)&& (CurrentData.EmployeesUK != ""))
                    {
                        
                        Employees_TMP_Heading.gameObject.SetActive(true);
                        Employees_TMP.GetComponent<RectTransform>().anchoredPosition = new Vector2(766, -829);
                    }
                    else
                    {
                        Employees_TMP_Heading.gameObject.SetActive(false);
                    }
                    Employees_TMP_Heading.text = "Employees:";
                    City_TMP_Heading.text = "City:";
                    Country_TMP_Heading.text = "Country:";
                    break;
                default:
                    break;
            }
        }
    }

    public void UpdateContents(DepartmentData myData, string myLanguage = "DK", bool myBackendMode = false)
    {

        CurrentData = myData;
        updateTextContents();

        bool ThereIsImages = false;
        if (myData.Images != null)
        {
            if (myData.Images.Count != 0)
            {
                ImageHandler.BuildContents(myData.ID);

                ThereIsImages = true;
            }

        }

        if (!ThereIsImages)
        {
            ImageHandler.NoImages();
        }


        if (myBackendMode)
        {
            //CloseButton.GetComponent<Button>().interactable = false;
            CloseButton.interactable = false;
        }
        else
        {
            //CloseButton.GetComponent<Button>().interactable = true;
            CloseButton.interactable = true;
        }
    }

    public void showImageNum(int myImageNum, long myVideoFrame)
    {
        ImageHandler.BuildContents(CurrentData.ID, myImageNum, myVideoFrame);
    }


}
