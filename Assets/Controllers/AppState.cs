﻿public static class AppState
{
    static bool m_ScreenSaverState = true;

    public static bool ScreenSaverState
    {
        get
        {
            return m_ScreenSaverState;
        }
        set
        {
            m_ScreenSaverState = value;
            if (onAppSatateChanged != null)
                onAppSatateChanged.Invoke(m_ScreenSaverState);
        }
    }

    public delegate void OnAppStateChanged(bool screenSaverState);
    public static event OnAppStateChanged onAppSatateChanged;

    public static void AddListener(OnAppStateChanged listener)
    {
        onAppSatateChanged += listener;
    }

    public static void RemoveListener(OnAppStateChanged listener)
    {
        onAppSatateChanged -= listener;
    }
}
