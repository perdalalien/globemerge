﻿using UnityEngine;
using UnityEngine.Events;

public class CloudController : MonoBehaviour
{
    [SerializeField] SQSQCloudHandling m_CloudHandler;
    [SerializeField] Transform globeContainer;
    [SerializeField] int m_ZoomLevelOnWhichCloudsAreVisible = 0;

    [SerializeField] UnityEvent m_OnCloundsShown;
    [SerializeField] UnityEvent m_OnCloundsHidden;

    bool m_CloudsVisible = true;
    int m_LastZoomLevel = 0;

    public void SetClouds(int zoomLevel)
    {
        if (zoomLevel == 0 && m_LastZoomLevel > 0 || zoomLevel > 0 && m_LastZoomLevel == 0)
        {
            m_CloudHandler.fadeCLouds();
            m_CloudsVisible = !m_CloudsVisible;

            if (m_CloudsVisible)
                m_OnCloundsShown.Invoke();
            else
                m_OnCloundsHidden.Invoke();
        }

        m_LastZoomLevel = zoomLevel;
    }
}
