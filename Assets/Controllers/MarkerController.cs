﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MarkerController : MonoBehaviour
{

    [System.Serializable]
    public class OnMarkerClicked : UnityEvent<Vector3> { }

    public class POIposition
    {
        public int ID { get; set; }

        public float Lat { get; set; }

        public float Lon { get; set; }
        public List<int> Departments { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

    }

    [SerializeField] float m_MarkerFadeStartAngle = 45f;
    [SerializeField] float m_MarkerFullFadeAngle = 70f;

    [SerializeField] ZoomController m_ZoomController;
    [SerializeField] Transform m_MarkerParent;
    [SerializeField] string m_DebugJSON;

    [SerializeField] float m_GlobeRadius = 100f;
    [SerializeField] float m_ScaleFactor = 15f;
    [SerializeField] List<Marker> m_Markers;
    [SerializeField] Marker m_MarkerPrefab;
    [SerializeField] float m_MarkerMergeDistance;

    [SerializeField] UnityEvent m_OnButtonZoomStarted;
    [SerializeField] UnityEvent m_OnButtonZoomEnded;

    [SerializeField] OnMarkerClicked m_OnMarkerClicked;

    public List<Marker> Markers { get => m_Markers; }

    HashSet<Marker> m_ProcessedMarkers;
    HashSet<int> m_SelectedMarkerCluster = new HashSet<int>();
    List<POIposition> m_POIPositions;

    Camera m_Camera;
    public Camera Camera { get => m_Camera; }

    Coroutine m_ZoomRoutine;

    public float MarkerScaleFactor { get => m_ScaleFactor; }
    public bool ShouldRecalculate { get; set; }

    public bool InCluster { get; set; }

    Marker m_LastSelectedMarker;
    public Marker LastSelectedMarker { get { return m_LastSelectedMarker; } }

    public ZoomController ZoomController { get => m_ZoomController; }

    float m_BaseMergeDistance;

    bool m_ButtonZoomActive;
    public bool ButtonZoomActive
    {
        get { return m_ButtonZoomActive; }
        set
        {
            m_ButtonZoomActive = value;
            if (m_ButtonZoomActive)
            {
                if (m_ZoomRoutine != null)
                    StopCoroutine(m_ZoomRoutine);

                m_ZoomRoutine = StartCoroutine(Zooming());
            }
            else
            {
                if (m_ZoomRoutine != null)
                    StopCoroutine(m_ZoomRoutine);
            }
        }
    }

    // Added By Per//
    public GameObject ManagerObject;
    // End Added by Per//


    void Start()
    {
        m_Camera = Camera.main;

        m_BaseMergeDistance = m_MarkerMergeDistance;

        m_ProcessedMarkers = new HashSet<Marker>();

        m_OnButtonZoomStarted.AddListener(SetZoomingOn);
        m_OnButtonZoomEnded.AddListener(SetZoomingOff);

        LoadPositions();
        SpawnMarkers();

        RecalculateMarkers();

        SetMarkersActive(false);

    }

    public void SetShouldRecalculate(bool shouldRecalculate)
    {
        ShouldRecalculate = shouldRecalculate;
    }

    private void OnDestroy()
    {
        m_OnButtonZoomStarted.RemoveListener(SetZoomingOn);
        m_OnButtonZoomEnded.RemoveListener(SetZoomingOff);
    }

    void SetZoomingOn()
    {
        ButtonZoomActive = true;
    }

    void SetZoomingOff()
    {
        ButtonZoomActive = false;
    }

    float GetMarkerAngleRelativeToCamera(Marker marker)
    {
        float angle = Vector3.Angle(Camera.transform.forward, -1f * (marker.transform.forward + marker.transform.position));
        return angle;
    }

    public float GetMarkerAlphaRelativeToCameraAngle(Marker marker)
    {
        if (m_ZoomController.CurrentZoomLevel == 0)
            return 0f;

        float angle = GetMarkerAngleRelativeToCamera(marker);
        if (angle > m_MarkerFadeStartAngle)
        {
            return Mathf.Clamp01(1f - ((angle - m_MarkerFadeStartAngle) / (m_MarkerFullFadeAngle - m_MarkerFadeStartAngle)));
        }
        else
        {
            return 1f;
        }
    }

    // DEBUG
    private void Update()
    {
        if ((ButtonZoomActive || ShouldRecalculate) && m_ZoomController.CurrentZoomLevel == 1 && !m_ZoomController.ZoomingOutOfCluster)
            RecalculateMarkers();

        RecalculateAlphaBasedOnCameraAngle();
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < m_Markers.Count; i++)
        {
            Vector3 from = -1f * m_Markers[i].transform.forward + m_Markers[i].transform.position;
            Gizmos.DrawLine(from, from * 10f);
        }
    }

    void RecalculateAlphaBasedOnCameraAngle()
    {
        if (m_ZoomController.CurrentZoomLevel != 1)
            return;

        if (InCluster || m_LastSelectedMarker != null)
            return;

        for (int i = 0; i < m_Markers.Count; i++)
        {
            m_Markers[i].SetAlphaInstant(GetMarkerAlphaRelativeToCameraAngle(m_Markers[i]));
        }
    }

    IEnumerator Zooming()
    {
        while (ButtonZoomActive)
        {
            m_MarkerMergeDistance *= 1f - Time.deltaTime;
            yield return null;
        }
    }

    public void SpawnMarkers(List<POIposition> positions)
    {
        DestroyMarkers();
        m_POIPositions.Clear();
        m_POIPositions.AddRange(positions);
        SpawnMarkers();
    }

    private void SpawnMarkers()
    {
        if (m_POIPositions == null || m_POIPositions.Count == 0)
            return;

        for (int i = 0; i < m_POIPositions.Count; i++)
        {
            m_Markers.Add(SpawnMarker(
                GetPostition(m_POIPositions[i].Lon, m_POIPositions[i].Lat),
                m_POIPositions[i],
                false));
        }
    }

    void DestroyMarkers()
    {
        if (m_Markers != null && m_Markers.Count > 0)
        {
            for (int i = m_Markers.Count - 1; i >= 0; i++)
            {
                Destroy(m_Markers[i].gameObject);
                m_Markers.RemoveAt(i);
            }
        }
    }

    public void ResetLastSelectedMarker()
    {
        m_LastSelectedMarker = null;
    }

    public void ResetMergeDistance()
    {
        if (!ButtonZoomActive && m_ZoomController.CurrentZoomLevel != 2)
            m_MarkerMergeDistance = m_BaseMergeDistance;
    }

    HashSet<Marker> clusterSet = new HashSet<Marker>();
    Marker[] cluster = new Marker[5];
    Marker largestDepartmentMarker = null;
    public void RecalculateMarkers()
    {
        m_ProcessedMarkers.Clear();

        SetMarkersActive(true);
        ResetMarkerValues(false);


        for (int i = 0; i < m_Markers.Count; i++)
        {
            if (m_ProcessedMarkers.Contains(m_Markers[i]))
                continue;

            clusterSet.Clear();
            m_Markers[i].GetClusterRecursive(clusterSet);

            if (clusterSet.Count > 0)
            {
                if (cluster.Length < clusterSet.Count)
                {
                    cluster = new Marker[clusterSet.Count];
                }
                else
                {
                    for (int j = 0; j < cluster.Length; j++)
                    {
                        cluster[j] = null;
                    }
                }
                clusterSet.CopyTo(cluster, 0);

                Marker largestDepartmentMarker = null;
                int totalDepartmentCount = 0;

                Vector3 cumulativePosition = Vector3.zero;

                foreach (var marker in cluster)
                {
                    if (marker == null)
                        continue;

                    totalDepartmentCount += marker.DepartmentCount;

                    if (largestDepartmentMarker == null)
                        largestDepartmentMarker = marker;
                    else
                    {
                        if (marker.DepartmentCount > largestDepartmentMarker.DepartmentCount)
                            largestDepartmentMarker = marker;
                    }

                    cumulativePosition += marker.transform.position;
                }

                largestDepartmentMarker.DepartmentCount = totalDepartmentCount;

                cumulativePosition /= (float)cluster.Length;

                foreach (var marker in cluster)
                {
                    if (marker == null)
                        continue;

                    m_ProcessedMarkers.Add(marker);
                    marker.SetAlpha(marker == largestDepartmentMarker && m_ZoomController.CurrentZoomLevel > 0 ? 1f : 0f, .33f);
                    marker.ClusterID = largestDepartmentMarker.ID;
                    //marker.MoveToPosition(cumulativePosition);
                }

                largestDepartmentMarker.ClusterID = largestDepartmentMarker.ID;
            }
            else
            {
                m_ProcessedMarkers.Add(m_Markers[i]);
            }
        }

        ShouldRecalculate = false;
    }

    public void ResetMarkerValues(bool updateUI)
    {
        for (int i = 0; i < m_Markers.Count; i++)
        {
            m_Markers[i].ResetDepartments(updateUI);
        }
    }

    public void SetMarkersActive(bool activeState)
    {
        for (int i = 0; i < m_Markers.Count; i++)
        {
            m_Markers[i].SetVisible(activeState);
        }
    }

    public void ResetMarkerScale()
    {
        for (int i = 0; i < m_Markers.Count; i++)
        {
            m_Markers[i].transform.localScale = Vector3.one * m_ScaleFactor;
        }
    }

    void LoadPositions()
    {
       // m_POIPositions = JsonConvert.DeserializeObject<List<POIposition>>(m_DebugJSON);

        // // Added By Per//
         string myPositionString = ManagerObject.GetComponent<dataHandling>().getFilteredPositionsAsString();
         m_POIPositions = JsonConvert.DeserializeObject<List<POIposition>>(myPositionString);
        // //End added by Per //


    }

    Marker SpawnMarker(Vector3 position, POIposition poiData, bool activeOnSpawn)
    {
        Marker marker = Instantiate(m_MarkerPrefab, m_MarkerParent);
        marker.name = "Marker_" + poiData.ID;

        marker.transform.position = position;
        marker.transform.localScale = Vector3.one * m_ScaleFactor;
        marker.transform.LookAt(Vector3.zero, new Vector3(0f, 1f, 0f));

        marker.Initialize(poiData.ID, poiData, m_OnButtonZoomStarted,
            m_OnButtonZoomEnded, m_OnMarkerClicked,
            (markerID) =>
            {
                m_LastSelectedMarker = markerID;
            }, position, this);

        marker.SpriteRenderer.enabled = false;
        return marker;
    }

    public Marker GetMarker(int id)
    {
        for (int i = 0; i < m_Markers.Count; i++)
        {
            if (m_Markers[i].ID == id)
                return m_Markers[i];
        }

        return null;
    }

    public void SetMarkersInteractable(bool interactable, bool affectAlpha = true)
    {
        if (m_ZoomController.CurrentZoomLevel != 2)
        {
            for (int i = 0; i < m_Markers.Count; i++)
            {
                m_Markers[i].Interactable = interactable;
                if (affectAlpha && m_Markers[i].ClusterID == m_Markers[i].ID)
                    m_Markers[i].SetAlpha(interactable ? 1f : 0f);
            }
        }
        else
        {
            if (m_ZoomController.CachedMarkerCluster.Count > 0)
            {
                for (int i = 0; i < m_Markers.Count; i++)
                {
                    bool clusterMarker = false;
                    for (int j = 0; j < m_ZoomController.CachedMarkerCluster.Count; j++)
                    {
                        if (m_Markers[i].ID == m_ZoomController.CachedMarkerCluster[j].ID)
                        {
                            clusterMarker = true;
                        }
                    }

                    m_Markers[i].Interactable = clusterMarker;
                    m_Markers[i].SetAlpha(m_Markers[i].Interactable ? 1f : 0f);
                }

                if (!InCluster)
                    m_ZoomController.CachedMarkerCluster.Clear();
            }
            else
            {
                for (int i = 0; i < m_Markers.Count; i++)
                {
                    m_Markers[i].Interactable = interactable;
                    m_Markers[i].SetAlpha(interactable ? 1f : 0f);
                }
            }
        }
    }

    public void ExitCluster()
    {
        InCluster = false;
        m_ZoomController.CachedMarkerCluster.Clear();
    }

    public void DeactivateNonSelectedMarkers(Marker selectedMarker)
    {
        clusterSet.Clear();
        selectedMarker.GetClusterRecursive(clusterSet);

        if (!clusterSet.Contains(selectedMarker))
            clusterSet.Add(selectedMarker);

        for (int i = 0; i < m_Markers.Count; i++)
        {
            bool markerFromCluster = m_Markers[i].ClusterID == selectedMarker.ClusterID;

            m_Markers[i].Interactable = markerFromCluster; // .ID == selectedMarker;
            m_Markers[i].SetAlpha(markerFromCluster ? 1f : 0f);
        }
    }

    public void DeactivateNonSelectedMarkers(int[] selectedMarkers)
    {
        m_SelectedMarkerCluster.Clear();
        for (int i = 0; i < selectedMarkers.Length; i++)
        {
            m_SelectedMarkerCluster.Add(selectedMarkers[i]);
        }

        for (int i = 0; i < m_Markers.Count; i++)
        {
            m_Markers[i].Interactable = m_SelectedMarkerCluster.Contains(m_Markers[i].ID);
            m_Markers[i].SetAlpha(m_SelectedMarkerCluster.Contains(m_Markers[i].ID) ? 1f : 0f);
        }
    }

    Vector3 GetPostition(float lattitude, float longitude)
    {
        var pos =
            Quaternion.AngleAxis(lattitude, -Vector3.up) *
            Quaternion.AngleAxis(longitude, -Vector3.right) *
            new Vector3(0, 0, 1) *
            m_GlobeRadius;


        return new Vector3(pos.x, pos.y, pos.z);
    }
}