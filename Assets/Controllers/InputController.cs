﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField] bool m_InputBlocked = true;
    [SerializeField] float m_InactivityTimeInSeconds = 30f;
    [SerializeField] ZoomController m_ZoomController;

    [SerializeField] float m_InactivityTime = 0f;

    //Per added//
    public GameObject Manager;

    void Start()
    {
        m_InactivityTimeInSeconds = Manager.GetComponent<dataHandling>().generalSettings.TimeOutSec;
    }
    //End Per added//

    public bool InputBocked { get { return m_InputBlocked; } }

    public bool GetMouseButtonDown(int index)
    {
        return !m_InputBlocked && Input.GetMouseButtonDown(index);
    }

    public bool GetMouseButton(int index)
    {
        return !m_InputBlocked && Input.GetMouseButton(index);
    }

    public bool GetMouseButtonUp(int index)
    {
        return !m_InputBlocked && Input.GetMouseButtonUp(index);
    }

    public void SetInputBlocked(bool isBlocked)
    {
        m_InputBlocked = isBlocked;
    }

    private void Awake()
    {
        AppState.AddListener(SetInputBlocked);
    }

    private void OnDestroy()
    {
        AppState.RemoveListener(SetInputBlocked);
    }

    private void Update()
    {
        if (AppState.ScreenSaverState &&
            Input.GetMouseButtonDown(0) &&
            !m_ZoomController.ZoomingCoroutineRunning &&
            !SQSQCloudHandling.cloudFading)
        {
            AppState.ScreenSaverState = false;
        }

        if (!AppState.ScreenSaverState)
        {
            if (Input.GetMouseButtonDown(0))
                m_InactivityTime = 0f;
            else
                m_InactivityTime += Time.deltaTime;
        }

        if (m_InactivityTime >= m_InactivityTimeInSeconds)
        {
            AppState.ScreenSaverState = true;
            m_InactivityTime = 0f;
        }
    }

    //Added by Per//
    public void ForceScreensaverDelay()
    {
        m_InactivityTime = 0f;
    }
    // end//
}
