﻿using UnityEngine;

public class TestBehaviour : MonoBehaviour
{

    // SCREEN SAVER DEMO ==========================

    // This doesn't HAVE to be done in awake. Any initialization function
    // that is used in your process
    private void Awake()
    {
        // Add listener so that each time the state changes
        // form screen saver and back to it this method gets called
        AppState.AddListener(DoStuff);
    }


    // Again, doesn't HAVE to be OnDestroy. Any "end the process" function will do
    private void OnDestroy()
    {
        // Remove the listener to avoid leaks, multiple calls etc
        AppState.RemoveListener(DoStuff);
    }

    public void DoStuff(bool screenSaverEntered)
    {
        if (screenSaverEntered)
        {
            // Do screen-saver stuff
        }
        else
        {
            // Do non-screen-saver stuff
        }
    }

    // SCREEN SAVER DEMO ==========================


    public void Test(int argument)
    {
        Debug.Log("TEST " + argument);
    }
}
