﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ZoomController : MonoBehaviour
{
    [System.Serializable]
    public class OnZoomStarted : UnityEvent<int> { }
    [System.Serializable]
    public class OnZoomEnded : UnityEvent<int> { }
    [System.Serializable]
    public class OnCenteredOnMarker : UnityEvent<int> { }

    [SerializeField] RectTransform m_CenteringTarget;
    [SerializeField] InputController m_InputController;
    [SerializeField] MarkerController m_MarkerController;
    [SerializeField] Transform m_Target;
    [SerializeField] Camera m_MainCamera;
    [SerializeField] float[] m_ZoomLevels;
    [SerializeField] int m_StartingZoomLevel;
    [SerializeField] OnZoomStarted onZoomStarted;
    [SerializeField] OnZoomEnded onZoomEnded;
    [SerializeField] GameObject m_ZoomOutButton;
    [SerializeField] OnCenteredOnMarker m_OnCenteredOnMarker;

    [SerializeField] AnimationCurve m_ShiftCurve;

    [SerializeField] UnityEvent m_OnCameraShiftStarted;
    [SerializeField] UnityEvent m_OnCameraShiftEnded;

    [SerializeField] float m_MoveSpeed = 2f;
    [SerializeField] float m_MaxCenteringStepZoomedIn = .03f;
    [SerializeField] float m_MaxCenteringStepZoomedOut = 1f;

    Coroutine m_ZoomRoutine;

    bool m_GradualZoomRequested;
    public bool GradualZoomRequested
    {
        get { return m_GradualZoomRequested; }
        set
        {
            m_GradualZoomRequested = value;
            if (m_GradualZoomRequested)
            {
                if (m_ZoomRoutine != null)
                    StopCoroutine(m_ZoomRoutine);

                m_ZoomRoutine = StartCoroutine(GradualZoom());
            }
            else
            {
                //if (m_ZoomRoutine != null)
                //    StopCoroutine(m_ZoomRoutine);
            }
        }
    }

    [SerializeField] int m_CurrentZoomLevel;
    public int CurrentZoomLevel { get { return m_CurrentZoomLevel; } }

    bool m_ZoomingRoutineRunning;
    public bool ZoomingCoroutineRunning { get { return m_ZoomingRoutineRunning; } }

    public bool ZoomingOutOfCluster { get; private set; }

    public void Awake()
    {
        SetZoomLevel(m_StartingZoomLevel);

        AppState.AddListener(HandleStateChange);
    }

    private void Update()
    {
        if (m_CurrentZoomLevel == 2)
        {
            m_InputController.SetInputBlocked(true);
        }

        m_ZoomOutButton.SetActive(!GradualZoomRequested && m_CurrentZoomLevel == 2);
    }

    public void HandleStateChange(bool isScreenSaver)
    {
        if (isScreenSaver)
        {
            Zoom(-2);
            m_MarkerController.SetMarkersActive(false);
            m_MarkerController.ExitCluster();
            m_MarkerController.ResetLastSelectedMarker();
            m_MarkerController.ShouldRecalculate = true;
            m_MarkerController.ResetMarkerScale();
        }
        else
            Zoom(1);
    }

    public void GoToZoomLevel(int level)
    {
        if (m_CurrentZoomLevel == level)
            return;

        Zoom(level - m_CurrentZoomLevel);
    }

    public void Zoom(int direction)
    {
        int pastZoom = m_CurrentZoomLevel;
        m_CurrentZoomLevel =
            Mathf.Clamp(m_CurrentZoomLevel + direction,
            0,
            m_ZoomLevels.Length - 1);

        if (pastZoom != m_CurrentZoomLevel &&
            !m_ZoomingRoutineRunning)
        {
            StartCoroutine(SmoothZoom(m_MainCamera.fieldOfView,
                                      m_ZoomLevels[m_CurrentZoomLevel]));

            onZoomStarted.Invoke(m_CurrentZoomLevel);

            if (m_CurrentZoomLevel != 2)
            {
                onZoomEnded.RemoveListener(OnZoomOutOfCluster);
                onZoomEnded.AddListener(OnZoomOutOfCluster);
            }
        }
    }

    void OnZoomOutOfCluster(int zoomLevel)
    {
        m_InputController.SetInputBlocked(false);
        m_MarkerController.ResetMergeDistance();
        m_MarkerController.SetMarkersInteractable(zoomLevel != 0);

        if (CurrentZoomLevel == 0)
            m_MarkerController.SetMarkersActive(false);

        m_MarkerController.ShouldRecalculate = true;

        onZoomEnded.RemoveListener(OnZoomOutOfCluster);
    }

    public void ShiftToPosition(Vector3 targetPosition)
    {
        Vector3 direction = (targetPosition - m_Target.position).normalized;

        Vector3 centeringPos =
            m_MainCamera.ScreenToWorldPoint(m_CenteringTarget.position);

        var newVector = Vector3.Reflect((m_Target.position - centeringPos),
                        (m_Target.position - m_MainCamera.transform.position)).normalized;

        float distance = Vector3.Distance(m_MainCamera.transform.position,
                                          m_Target.position);
        StartCoroutine(SmoothMoveToPosition(direction * distance));
    }

    public List<Marker> CachedMarkerCluster = new List<Marker>();
    public List<Marker> MarkerCluster = new List<Marker>();

    public static float m_SmoothMoveProgress = 0f;
    bool clickedCluster = false;
    int[] markerIDs;

    IEnumerator SmoothMoveToPosition(Vector3 targetPosition)
    {
        m_OnCameraShiftStarted.Invoke();
        var marker = m_MarkerController.LastSelectedMarker;
        clickedCluster = false;
        if (marker != null)
            clickedCluster = marker.TouchCount > 0;

        MarkerCluster.Clear();// = new Marker[marker.Cluster.Count];
        CachedMarkerCluster.Clear();
        HashSet<Marker> clusterSet = new HashSet<Marker>();
        marker.GetClusterRecursive(clusterSet);
        foreach (var m in clusterSet) //.CopyTo(MarkerCluster);
        {
            if (!CachedMarkerCluster.Contains(m))
                CachedMarkerCluster.Add(m);
            MarkerCluster.Add(m);
        }

        for (int i = 0; i < m_MarkerController.Markers.Count; i++)
        {
            if (m_MarkerController.Markers[i] != null &&
               m_MarkerController.Markers[i].ClusterID == m_MarkerController.LastSelectedMarker.ClusterID &&
               !CachedMarkerCluster.Contains(m_MarkerController.Markers[i]))
                CachedMarkerCluster.Add(m_MarkerController.Markers[i]);
        }

        if (!CachedMarkerCluster.Contains(marker))
            CachedMarkerCluster.Add(marker);

        markerIDs = new int[MarkerCluster.Count + 1];
        for (int i = 0; i < MarkerCluster.Count; i++)
        {
            markerIDs[i] = MarkerCluster[i].ID;
        }
        markerIDs[MarkerCluster.Count] = marker.ID;
        Vector3 initialPosition = m_MainCamera.transform.position;

        float distance = Vector3.Distance(initialPosition, targetPosition);

        m_SmoothMoveProgress = 0f;
        while (m_SmoothMoveProgress < 1f)
        {
            m_MainCamera.transform.position =
                Vector3.Lerp(initialPosition, targetPosition,
                    m_ShiftCurve.Evaluate(m_SmoothMoveProgress));

            m_MainCamera.transform.LookAt(m_Target);
            m_SmoothMoveProgress += Mathf.Clamp(Time.deltaTime * m_MoveSpeed / distance,
                0f,
                CurrentZoomLevel == 2 ? m_MaxCenteringStepZoomedIn :
                m_MaxCenteringStepZoomedOut);
            yield return null;
        }

        if (!GradualZoomRequested)
        {
            if (clickedCluster)
            {
                m_MarkerController.
                    DeactivateNonSelectedMarkers(
                        markerIDs);
            }
            else
            {
                m_MarkerController.
                    DeactivateNonSelectedMarkers(
                        m_MarkerController.LastSelectedMarker);
                m_OnCenteredOnMarker.Invoke(m_MarkerController.LastSelectedMarker.ID);
            }

            m_InputController.SetInputBlocked(true);
        }

        m_SmoothMoveProgress = 1f;
        m_MainCamera.transform.position =
                Vector3.Lerp(initialPosition, targetPosition, m_SmoothMoveProgress);

        m_MainCamera.transform.LookAt(m_Target);

        m_OnCameraShiftEnded.Invoke();

    }

    float Sigmoid(float x)
    {
        x = x / 2f - .5f;
        return (1f / (1 + Mathf.Exp(-x)));
    }

    void SetZoomLevel(int zoomLevel)
    {
        zoomLevel =
            Mathf.Clamp(zoomLevel,
            0,
            m_ZoomLevels.Length - 1);
        m_MainCamera.fieldOfView = m_ZoomLevels[zoomLevel];
    }

    public void SetGradualZoomRequest(bool isRequested)
    {
        GradualZoomRequested = isRequested;
    }

    IEnumerator GradualZoom()
    {
        m_CurrentZoomLevel = 2;
        float initialFOV = m_MainCamera.fieldOfView;

        if (clickedCluster)
        {
            m_MarkerController.
                DeactivateNonSelectedMarkers(
                    markerIDs);
        }
        else
        {
            m_MarkerController.
                DeactivateNonSelectedMarkers(
                    m_MarkerController.LastSelectedMarker);
            m_OnCenteredOnMarker.Invoke(m_MarkerController.LastSelectedMarker.ID);
        }

        while (GradualZoomRequested)
        {
            while (m_SmoothMoveProgress < .9f)
                yield return null;
            m_MainCamera.fieldOfView *= 1f - Time.deltaTime;
            yield return null;
        }

        m_InputController.SetInputBlocked(true);

    }

    Dictionary<Marker, float> clusterScales = new Dictionary<Marker, float>();
    IEnumerator SmoothZoom(float currentFOV, float targetFOV)
    {
        m_ZoomingRoutineRunning = true;

        ZoomingOutOfCluster = CachedMarkerCluster.Count > 0 && currentFOV < targetFOV;

        clusterScales.Clear();

        if (CurrentZoomLevel == 0)
            m_MarkerController.SetMarkersActive(false);

        if (ZoomingOutOfCluster)
        {
            foreach (var marker in CachedMarkerCluster)
            {
                clusterScales.Add(marker, marker.transform.localScale.x);
                marker.IsInTheInspectedCluster = false;
                marker.IsExitingTheInspectedCluster = true;
            }
        }

        float progress = 0f;
        while (progress < 1f)
        {
            m_MainCamera.fieldOfView =
                Mathf.Lerp(currentFOV, targetFOV, progress);

            if (ZoomingOutOfCluster)
            {
                foreach (var item in clusterScales)
                {
                    if (item.Key != null)
                        item.Key.transform.localScale =
                            Vector3.Lerp(Vector3.one * item.Value, Vector3.one * m_MarkerController.MarkerScaleFactor, progress);
                }
            }

            progress += Time.deltaTime;
            yield return null;
        }

        progress = 1f;
        m_MainCamera.fieldOfView =
                Mathf.Lerp(currentFOV, targetFOV, progress);


        foreach (var item in clusterScales)
        {
            if (item.Key != null)
                item.Key.IsExitingTheInspectedCluster = false;
        }

        if (CurrentZoomLevel < 2)
            m_MarkerController.ResetMarkerScale();

        ZoomingOutOfCluster = false;
        m_ZoomingRoutineRunning = false;
        onZoomEnded.Invoke(m_CurrentZoomLevel);

        yield return null;
        CachedMarkerCluster.Clear();
    }
}
