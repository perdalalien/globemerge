﻿using UnityEngine;
using UnityEngine.UI;

public class ZoomButtonHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public Image ButtonImage;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ButtonImage.color = new Color(1f, 1f, 1f, (Mathf.PingPong(Time.time, 1.5f) * 0.7f) + 0.3f);
    }
}
