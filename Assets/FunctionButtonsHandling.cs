﻿using UnityEngine;
using UnityEngine.UI;

public class FunctionButtonsHandling : MonoBehaviour
{

    public GameObject Manager;


    public GameObject MuteButton;
    public Sprite MuteNormTexture;
    public Sprite MuteSelectedTexture;

    private bool isMuted = false;


    private void Awake()
    {
        AppState.AddListener(ScreensaverTurnOnOff);
    }
    void Start()
    {
        MuteButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ScreensaverTurnOnOff(bool screensaverStarted)
    {
        if (screensaverStarted)
        {
            if (isMuted)
            {
                MuteButtonPressed();
            }
            MuteButton.SetActive(false);
        }
        else
        {
            MuteButton.SetActive(true);
        }
    }

    public void MuteButtonPressed()
    {
        isMuted = !isMuted;
        if (isMuted)
        {
            MuteButton.GetComponent<Image>().sprite = MuteSelectedTexture;
            AudioListener.pause = true;
            AudioListener.volume = 0;

        }
        else
        {
            MuteButton.GetComponent<Image>().sprite = MuteNormTexture;
            AudioListener.pause = false;
            AudioListener.volume = 1;
        }


    }
}
