﻿using UnityEngine;

public class EarthMaterialDefiner : MonoBehaviour
{
    // Start is called before the first frame update

    public float normal_strength = 0.5f;
    public float main_brightness = 0.5f;
    public float light_scale = 0.5f;
    public float reflection_shine = 0.22f;
    public Color32 reflection_color = Color.white;

    public Color32 AtmosNearColor = new Color(0.168f, 0.737f, 1f);
    public Color32 AtmosFarColor = new Color(0.455f, 0.518f, 0.9850f);
    public float AtmosFallOff = 3f;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateMaterial(GameObject myEarthObject)
    {
        StagitMaterialChanger myMaterialScript = myEarthObject.GetComponentInChildren<StagitMaterialChanger>();
        myMaterialScript.reflection_color = reflection_color;
        myMaterialScript.normal_strength = normal_strength;
        myMaterialScript.main_brightness = main_brightness;
        myMaterialScript.light_scale = light_scale;
        myMaterialScript.reflection_shine = reflection_shine;
        myMaterialScript.AtmosNearColor = AtmosNearColor;
        myMaterialScript.AtmosFarColor = AtmosFarColor;
        myMaterialScript.AtmosFallOff = AtmosFallOff;
        myMaterialScript.SetMaterial = true;

    }
}
