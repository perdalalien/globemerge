﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] float scrollingAngleThreshold = 45f;
    [SerializeField] float verticalAngleLimit = 25;
    [SerializeField] float CameraDistance = 630;
    [SerializeField] Transform testTransform;
    [SerializeField] Transform leadTransform;
    [SerializeField] InputController inputController;

    [Range(.1f, 10f)]
    [SerializeField]
    float rubberBand = 3.5f;

    [Range(.01f, 100f)]
    [SerializeField]
    float snapDistance = 1f;

    private const int SpehreRadius = 1;
    private Vector3? _mouseStartPos;
    private Vector3? _currentMousePos;

    float standardDistance;

    void Start()
    {
        // init the camera to look at this object
        Vector3 cameraPos = new Vector3(
            transform.position.x,
            transform.position.y,
            transform.position.z - CameraDistance);

        Camera.main.transform.position = cameraPos;
        Camera.main.transform.LookAt(transform.position);

        standardDistance = Vector3.Distance(Camera.main.transform.position, transform.position);

        leadTransform.position = testTransform.position = Camera.main.transform.position;
        leadTransform.rotation = testTransform.rotation = Camera.main.transform.rotation;

    }

    Vector2 m_LastTouchScreenPos;
    Vector2 m_CurrentTouchScreenPos;
    [SerializeField] float m_ScreenPointDistanceMargin;

    private void Update()
    {
        if (inputController.InputBocked)
        {
            HandleDrop();
            leadTransform.position = testTransform.position = Camera.main.transform.position;
            leadTransform.rotation = testTransform.rotation = Camera.main.transform.rotation;
            return;
        }

        m_CurrentTouchScreenPos = Input.mousePosition;

        float distance = Vector2.Distance(m_CurrentTouchScreenPos,
            m_LastTouchScreenPos);

        if (inputController.GetMouseButtonDown(0) ||
            (inputController.GetMouseButton(0) &&
            distance > m_ScreenPointDistanceMargin &&
            _mouseStartPos == null))
        {
            _mouseStartPos = GetMouseHit();
        }
        if (_mouseStartPos != null && distance > m_ScreenPointDistanceMargin)
            HandleDrag();
        else HandleDrop();
        if (inputController.GetMouseButtonUp(0)) HandleDrop();

        m_LastTouchScreenPos = m_CurrentTouchScreenPos;
    }

    Vector3 velocity;
    private void LateUpdate()
    {
        if (inputController.InputBocked)
        {
            HandleDrop();
            leadTransform.position = testTransform.position = Camera.main.transform.position;
            leadTransform.rotation = testTransform.rotation = Camera.main.transform.rotation;
            return;
        }

        float distance = Vector3.Distance(
                Camera.main.transform.position,
                leadTransform.position
            );

        Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position,
            leadTransform.position, distance > snapDistance ? distance / rubberBand : distance);
        Camera.main.transform.position =
            (Camera.main.transform.position - transform.position).normalized *
            standardDistance;
        Camera.main.transform.LookAt(transform.position);
    }

    public void InjectMovementValues(float rubberBandValue, float SnapDistanceValue, float ScreenPointDistanceValue)
    {
        rubberBand = rubberBandValue;
        snapDistance = SnapDistanceValue;
        m_ScreenPointDistanceMargin = ScreenPointDistanceValue;
    }
    public void OnDrawGizmos()
    {
        if (_currentMousePos != null && _mouseStartPos != null)
        {
            Vector3 projectedStart = Vector3.ProjectOnPlane((Vector3)(_mouseStartPos - transform.position) * 50f, Vector3.up);
            Vector3 projectedCurrent = Vector3.ProjectOnPlane((Vector3)(_currentMousePos - transform.position) * 50f, Vector3.up);
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(transform.position, projectedStart);
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, projectedCurrent);
        }
    }

    private void HandleDrag()
    {
        _currentMousePos = GetMouseHit();
        if (_currentMousePos != null)
        {
            Vector3 currentTouchPos = (Vector3)_currentMousePos;
            Vector3 currentTouchDir = currentTouchPos.normalized;
            Vector3 cameraDir = Camera.main.transform.position.normalized;

            if (Vector3.Angle(currentTouchDir, cameraDir) <
                    scrollingAngleThreshold)
                RotateCamera((Vector3)_mouseStartPos, currentTouchPos);
            else
                HandleDrop();
        }
    }

    private void HandleDrop()
    {
        _mouseStartPos = null;
        _currentMousePos = null;
    }

    private void RotateCamera(Vector3 dragStartPosition, Vector3 dragEndPosition)
    {
        // in case the spehre model is not a perfect sphere..
        dragEndPosition = dragEndPosition.normalized * SpehreRadius;
        dragStartPosition = dragStartPosition.normalized * SpehreRadius;
        // calc a vertical vector to rotate around..
        var cross = Vector3.Cross(dragEndPosition, dragStartPosition);
        // calc the angle for the rotation..
        var angle = Vector3.SignedAngle(dragEndPosition, dragStartPosition, cross);

        Vector3 projectedStart = Vector3.ProjectOnPlane(dragStartPosition,
            Vector3.Cross(leadTransform.forward, transform.up));
        Vector3 projectedEnd = Vector3.ProjectOnPlane(dragEndPosition,
            Vector3.Cross(leadTransform.forward, transform.up));

        bool goingUp = Vector3.SignedAngle(projectedStart, projectedEnd,
            Vector3.Cross(leadTransform.forward, transform.up)) > 0f;

        testTransform.RotateAround(transform.position, cross, angle);
        // roatate around the vector..

        float clampedAngle = (testTransform.localEulerAngles.x > 270f) ?
                Mathf.Clamp(testTransform.localEulerAngles.x, 270f + verticalAngleLimit, 360f) :
                Mathf.Clamp(testTransform.localEulerAngles.x, 0f, 90f - verticalAngleLimit);

        testTransform.localEulerAngles = new Vector3
            (
                clampedAngle,
                leadTransform.localEulerAngles.y,
                0f
            );

        Vector3 prevPos = testTransform.localPosition;
        Vector3 prevRot = testTransform.localEulerAngles;

        testTransform.LookAt(transform.position);

        bool canRotate = false;

        if ((leadTransform.localEulerAngles.x > 260f &&
            leadTransform.localEulerAngles.x < 270f + verticalAngleLimit && !goingUp) ||
            leadTransform.localEulerAngles.x < 100f &&
            leadTransform.localEulerAngles.x > 90f - verticalAngleLimit && goingUp)
            canRotate = false;
        else
            canRotate = true;

        if (canRotate)
        {
            leadTransform.RotateAround(transform.position, cross, angle);
            leadTransform.LookAt(transform.position);
        }
        else
        {
            Vector3 projectedStartHoriz = Vector3.ProjectOnPlane(dragStartPosition, Vector3.up);
            Vector3 projectedCurrentHoriz = Vector3.ProjectOnPlane(dragEndPosition, Vector3.up);
            Vector3 crossUp = Vector3.Cross(
                        projectedStartHoriz,
                        projectedCurrentHoriz
                    );
            leadTransform.RotateAround(transform.position, crossUp,
                -Vector3.SignedAngle(projectedStartHoriz, projectedCurrentHoriz, crossUp));

            leadTransform.LookAt(transform.position);
        }

        testTransform.position = Camera.main.transform.position;
        testTransform.rotation = Camera.main.transform.rotation;
    }

    /**
     * Projects the mouse position to the sphere and returns the intersection point. 
     */
    private Vector3? GetMouseHit()
    {
        // make sure there is a shepre mesh with a colider centered at this game object
        // with a radius of SpehreRadius
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            return hit.point;
        }
        return null;
    }
}