﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MouseDragEvent : UnityEvent<Vector3> { }

public class InputManager : MonoBehaviour
{
    [SerializeField] MouseDragEvent mouseDragEvent = new MouseDragEvent();

    Vector3 lastMousePos;

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 delta = lastMousePos - Input.mousePosition;
            lastMousePos = Input.mousePosition;
            mouseDragEvent.Invoke(delta);
        }
    }
}
